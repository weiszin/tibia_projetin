# -*- coding: utf-8 -*-

import bs4
import urllib.request

def func(namePage, nameOut, idx):
    f = urllib.request.urlopen("https://www.tibiawiki.com.br/wiki/" + namePage)

    soup = bs4.BeautifulSoup(f.read(), 'html.parser')

    # print(soup.encode("utf-8"))
    # print(soup.find("td").find_next_sibling("td").text)

    data = []
    table = soup.findAll('table', {'class': 'sortable'})
    table_body = table[idx].find('tbody')

    rows = table_body.find_all('tr')
    for row in rows:
        cols = row.find_all('td')
        cols = [ele.text.strip() for ele in cols]
        data.append([ele for ele in cols])

    # Interesse:
    # Name, Defense Weight, Slots, Skill bonus, protection, level, vocation
    # 0, 2, 3, 4, 7, 8, 9, 10

    listaCols = [0, 2, 3, 4]

    def ajustaData(data, idx):
        data[idx] = data[idx].split(',')
        for i in range(len(data[idx])):
            data[idx][i] = data[idx][i].replace(' ', '').replace("%", '')
            if('+' in data[idx][i]):
                data[idx][i] = data[idx][i].split('+')
                data[idx][i][1] = '+' + data[idx][i][1]
            elif('-' in data[idx][i]):
                data[idx][i] = data[idx][i].split('-')
                data[idx][i][1] = '-' + data[idx][i][1]

    def ajustaName(data):
        data[0] = data[0].replace(' ', '_')

    with open(nameOut, 'w') as file:
        for i in range(1, len(data)):
            if(data[i][3] != ''):
                ajustaData(data[i], 3)
            ajustaName(data[i])
            toFile = ''
            for atr in listaCols:
                if(atr != 0): toFile += ' '
                if(str(data[i][atr]) == '' or str(data[i][atr]) == '[\'\']'):
                    toFile += "null"
                else:
                    if(atr == 3):
                        toFile += str(len(data[i][atr]))
                        for x in data[i][atr]:
                            toFile += " " + str(x).replace('[', '').replace(']', '').replace('\'', '').replace(",", '')
                    else:
                        toFile += str(data[i][atr])
            toFile += "\n"
            file.write(toFile)

func("Muni%C3%A7%C3%A3o", "Arrows", 0)
func("Muni%C3%A7%C3%A3o", "Bolts", 1)
