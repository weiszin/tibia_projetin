# -*- coding: utf-8 -*-

import bs4
import urllib.request

def func(nameOut, idx):
    f = urllib.request.urlopen("https://www.tibiawiki.com.br/wiki/Dist%C3%A2ncia")
    soup = bs4.BeautifulSoup(f.read(), 'html.parser')

    data = []
    table = soup.findAll('table', {'class': 'sortable'})

    table_body = table[idx].find('tbody')

    rows = table_body.find_all('tr')
    for row in rows:
        cols = row.find_all('td')
        cols = [ele.text.strip() for ele in cols]
        data.append([ele for ele in cols])

    # Interesse:
    # Name, Defense Weight, Slots, Skill bonus, protection, level, vocation
    # 0, 2, 3, 4, 7, 8, 9, 10

    listaCols = [0, 2, 3, 4, 5, 6, 7, 8]

    def ajustaName(data):
        data[0] = data[0].replace(' ', '_')

    def ajustaHands(data, idx):
        data[idx] = "1" if data[idx] == "Uma" else "2"

    # Nome, Level, ataque, elementAttack, skillBonus, attackModifier, hits, hands, weight, slots, range

    with open(nameOut, 'w') as file:
        for i in range(1, len(data)):
            ajustaName(data[i])
            ajustaHands(data[i], 5)
            toFile = ''
            for atr in listaCols:
                if(atr != 0): toFile += ' '
                if(atr == 3):
                    if(idx == 0):
                        toFile += str(data[i][atr]) + " null null null"
                    else:
                        if(data[i][atr] == ''):
                            toFile += "null null null null"
                        else:
                            toFile += "null null null " + str(data[i][atr])
                elif(str(data[i][atr]) == '' or str(data[i][atr]) == '[\'\']'):
                    toFile += "null"
                else:
                    toFile += str(data[i][atr])
            toFile += "\n"
            file.write(toFile)

THROWABLE = 0
CROSSBOW = 1
BOW = 2

func("Throwables", THROWABLE)
func("Crossbows", CROSSBOW)
func("Bows", BOW)
