# -*- coding: utf-8 -*-

import bs4
import urllib.request

def func(namePage, nameOut):
    f = urllib.request.urlopen("https://www.tibiawiki.com.br/wiki/" + namePage)
    soup = bs4.BeautifulSoup(f.read(), 'html.parser')

    data = []
    table = soup.find('table', {'class': 'sortable'})

    table_body = table.find('tbody')

    rows = table_body.find_all('tr')
    for row in rows:
        cols = row.find_all('td')
        cols = [ele.text.strip() for ele in cols]
        data.append([ele for ele in cols])

    # Interesse:
    # Name, level, element, damage, mana_per_attacl, slots, attributes, weight
    # 0, 2, 3, 4, 5, 6, 7, 8

    listaCols = [0, 2, 3, 4, 5, 6, 7, 8]

    def ajustaName(data):
        data[0] = data[0].replace(' ', '_')

    def ajustaSkillBonus(data, idx):
        data[idx] = data[idx].split(',')
        for i in range(len(data[idx])):
            data[idx][i] = data[idx][i].replace(' ', '').replace("%", '')
            if('+' in data[idx][i]):
                data[idx][i] = data[idx][i].split('+')
                data[idx][i][1] = '+' + data[idx][i][1]
            elif('-' in data[idx][i]):
                data[idx][i] = data[idx][i].split('-')
                data[idx][i][1] = '-' + data[idx][i][1]

    with open(nameOut, 'w') as file:
        for i in range(1, len(data)):
            ajustaName(data[i])
            ajustaSkillBonus(data[i], 7)
            toFile = ''
            for atr in listaCols:
                if(atr != 0): toFile += ' '
                if(str(data[i][atr]) == '' or str(data[i][atr]) == '[\'\']'):
                    toFile += "null"
                else:
                    if(atr == 7):
                        toFile += str(len(data[i][atr]))
                        for x in data[i][atr]:
                            toFile += " " + str(x).replace('[', '').replace(']', '').replace('\'', '').replace(",", '')
                    else:
                        toFile += str(data[i][atr])
            if(namePage == "Wands"):
                toFile += " Sorcerers\n"
            else:
                toFile += " Druids\n"
            file.write(toFile)


func("Wands", "Wands")
func("Rods", "Rods")
