# -*- coding: utf-8 -*-

import bs4
import urllib.request

f = urllib.request.urlopen("https://www.tibiawiki.com.br/wiki/Escudos")

soup = bs4.BeautifulSoup(f.read(), 'html.parser')

# print(soup.encode("utf-8"))
# print(soup.find("td").find_next_sibling("td").text)

data = []
table = soup.find('table', {'class': 'sortable'})
table_body = table.find('tbody')

rows = table_body.find_all('tr')
for row in rows:
    cols = row.find_all('td')
    cols = [ele.text.strip() for ele in cols]
    data.append([ele for ele in cols])

# Interesse:
# Name, Defense Weight, Slots, Skill bonus, protection, level, vocation
# 0, 2, 3, 4, 7, 8, 9, 10

listaCols = [0, 2, 3, 4, 7, 8, 9, 10]

def ajustaData(data, idx):
    data[idx] = data[idx].split(',')
    for i in range(len(data[idx])):
        data[idx][i] = data[idx][i].replace(' ', '').replace("%", '')
        if('+' in data[idx][i]):
            data[idx][i] = data[idx][i].split('+')
            data[idx][i][1] = '+' + data[idx][i][1]
        else:
            data[idx][i] = data[idx][i].split('-')
            data[idx][i][1] = '-' + data[idx][i][1]

def ajustaVocation(data):
    data[10] = data[10].split("and")
    for i in range(len(data[10])):
        data[10][i] = data[10][i].replace(' ', '')

def ajustaName(data):
    data[0] = data[0].replace(' ', '_')

with open('Shields', 'w') as file:
    for i in range(1, len(data)):
        if(data[i][7] != ''):
            ajustaData(data[i], 7)
        if(data[i][8] != ''):
            ajustaData(data[i], 8)
        ajustaVocation(data[i])
        ajustaName(data[i])
        toFile = ''
        for atr in listaCols:
            if(atr != 0): toFile += ' '
            if(str(data[i][atr]) == '' or str(data[i][atr]) == '[\'\']'):
                toFile += "null"
            else:
                if(atr == 7 or atr == 8 or atr == 10):
                    toFile += str(len(data[i][atr]))
                    for x in data[i][atr]:
                        toFile += " " + str(x).replace('[', '').replace(']', '').replace('\'', '').replace(",", '')
                else:
                    toFile += str(data[i][atr])
        toFile += "\n"
        file.write(toFile)
