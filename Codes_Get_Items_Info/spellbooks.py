# -*- coding: utf-8 -*-

import bs4
import urllib.request

f = urllib.request.urlopen("https://www.tibiawiki.com.br/wiki/Spellbooks")

soup = bs4.BeautifulSoup(f.read(), 'html.parser')

# print(soup.encode("utf-8"))
# print(soup.find("td").find_next_sibling("td").text)

data = []
table = soup.find('table', {'class': 'sortable'})
table_body = table.find('tbody')

rows = table_body.find_all('tr')
for row in rows:
    cols = row.find_all('td')
    cols = [ele.text.strip() for ele in cols]
    data.append([ele for ele in cols])

# Interesse:
# Name, Defense Weight, Slots, Skill bonus, protection, level, vocation
# 0, 2, 3, 4, 7, 8, 9, 10

listaCols = [0, 4, 7, 8, -1, 5, 6, 2]

def ajustaData(data, idx):
    data[idx] = data[idx].split(',')
    for i in range(len(data[idx])):
        data[idx][i] = data[idx][i].replace(' ', '').replace("%", '')
        if('+' in data[idx][i]):
            data[idx][i] = data[idx][i].split('+')
            data[idx][i][1] = '+' + data[idx][i][1]
        elif('-' in data[idx][i]):
            data[idx][i] = data[idx][i].split('-')
            data[idx][i][1] = '-' + data[idx][i][1]

def ajustaName(data):
    data[0] = data[0].replace(' ', '_')

with open('Spellbooks', 'w') as file:
    for i in range(1, len(data)):
        if(data[i][5] != ''):
            ajustaData(data[i], 5)
        if(data[i][6] != ''):
            ajustaData(data[i], 6)
        ajustaName(data[i])
        toFile = ''
        for atr in listaCols:
            if(atr != 0): toFile += ' '
            if(atr == -1 or str(data[i][atr]) == '' or str(data[i][atr]) == '[\'\']'):
                toFile += "null"
            else:
                if(atr == 5 or atr == 6):
                    toFile += str(len(data[i][atr]))
                    for x in data[i][atr]:
                        toFile += " " + str(x).replace('[', '').replace(']', '').replace('\'', '').replace(",", '')
                else:
                    toFile += str(data[i][atr])
        toFile += " 2 Sorcerers Druids\n"
        file.write(toFile)
