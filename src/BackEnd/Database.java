/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package BackEnd;

import Classes.Ammo;
import Classes.DefenseSet;
import Classes.DistanceWeapon;
import Classes.MagicWeapon;
import Classes.MeleeWeapon;
import Classes.RingAmulet;
import Classes.Shield;
import Enums.Equipments;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;

/**
 *
 * @author Weiss
 */
public final class Database {
    private static final Database INSTANCE = new Database();
    
    private static ArrayList<Shield> shields;
    private static ArrayList<Shield> spellbooks;
    private static ArrayList<MeleeWeapon> swords;
    private static ArrayList<MeleeWeapon> axes;
    private static ArrayList<MeleeWeapon> clubs;
    private static ArrayList<DistanceWeapon> throwables;
    private static ArrayList<DistanceWeapon> bows;
    private static ArrayList<DistanceWeapon> crossbows;
    private static ArrayList<MagicWeapon> wands;
    private static ArrayList<MagicWeapon> rods;
    private static ArrayList<DefenseSet> helmets;
    private static ArrayList<DefenseSet> armors;
    private static ArrayList<DefenseSet> legs;
    private static ArrayList<DefenseSet> boots;
    private static ArrayList<RingAmulet> amulets;
    private static ArrayList<RingAmulet> rings;
    private static ArrayList<Ammo> arrows;
    private static ArrayList<Ammo> bolts;
    
    
    private Database(){}
    public static Database getInstance(){
        return INSTANCE;
    }
    
    public void loadShield(String path, Equipments typeWeapon) throws FileNotFoundException, IOException{
        switch(typeWeapon){
            case SHIELD:
                this.shields = DatabaseAux.loadShields(path, typeWeapon);
                break;
            case SPELLBOOK:
                this.spellbooks = DatabaseAux.loadShields(path, typeWeapon);
                break;                
        }        
    }    
    public void loadMeleeWeapons(String path, Equipments typeWeapon) throws IOException{
        switch(typeWeapon){
            case WEAPON_SWORD:
                this.swords = DatabaseAux.loadMeleeWeapons(path, typeWeapon);
                break;
            case WEAPON_AXE:
                this.axes = DatabaseAux.loadMeleeWeapons(path, typeWeapon);
                break;
            case WEAPON_CLUB:
                this.clubs = DatabaseAux.loadMeleeWeapons(path, typeWeapon);
                break;
        }
    }
    public void loadDistanceWeapons(String path, Equipments typeWeapon) throws IOException{
        switch(typeWeapon){
            case WEAPON_THROWABLE:
                this.throwables = DatabaseAux.loadDistanceWeapons(path, typeWeapon);
                break;
            case WEAPON_BOW:
                this.bows = DatabaseAux.loadDistanceWeapons(path, typeWeapon);
                break;
            case WEAPON_CROSSBOW:
                this.crossbows = DatabaseAux.loadDistanceWeapons(path, typeWeapon);
                break;
        }
    }
    public void loadMagicWeapons(String path, Equipments typeWeapon) throws FileNotFoundException{
        switch(typeWeapon){
            case WEAPON_WAND:
                this.wands = DatabaseAux.loadMagicWeapons(path, typeWeapon);
                break;
            case WEAPON_ROD:
                this.rods = DatabaseAux.loadMagicWeapons(path, typeWeapon);
                break;
        }
    }
    public void loadDefenseSet(String path, Equipments typeWeapon) throws FileNotFoundException {
        switch(typeWeapon){
            case HELMET:
                this.helmets = DatabaseAux.loadDefenseSet(path, typeWeapon);
                break;
            case ARMOR:
                this.armors = DatabaseAux.loadDefenseSet(path, typeWeapon);
                break;
            case LEGS:
                this.legs = DatabaseAux.loadDefenseSet(path, typeWeapon);
                break;
            case BOOTS:
                this.boots = DatabaseAux.loadDefenseSet(path, typeWeapon);
                break;
        }
    }
    public void loadRingNecklace(String path, Equipments typeWeapon) throws FileNotFoundException, IOException{
        switch(typeWeapon){
            case AMULET:
                Database.amulets = DatabaseAux.loadRingAmulet(path, typeWeapon);
                break;
            case RING:
                Database.rings = DatabaseAux.loadRingAmulet(path, typeWeapon);
                break;
        }
    }
    public void loadAmmo(String path, Equipments typeWeapon) throws UnsupportedEncodingException, IOException{
        switch(typeWeapon){
            case AMMO_BOLT:
                Database.bolts = DatabaseAux.loadAmmo(path, typeWeapon);
                break;
            case AMMO_ARROW:
                Database.arrows = DatabaseAux.loadAmmo(path, typeWeapon);
                break;
        }
    }
    
    public static ArrayList<Shield> getShields(){
        return Database.shields;
    }
    public static ArrayList<Shield> getSpellbooks(){
        return Database.spellbooks;
    }    
    public static ArrayList<MeleeWeapon> getSwords(){
        return Database.swords;
    }
    public static ArrayList<MeleeWeapon> getAxes(){
        return Database.axes;
    }
    public static ArrayList<MeleeWeapon> getClubs(){
        return Database.clubs;
    }
    public static ArrayList<DistanceWeapon> getThrowables(){
        return Database.throwables;
    }
    public static ArrayList<DistanceWeapon> getBows(){
        return Database.bows;
    }
    public static ArrayList<DistanceWeapon> getCrossbows(){
        return Database.crossbows;
    }
    public static ArrayList<MagicWeapon> getWands(){
        return Database.wands;
    }
    public static ArrayList<MagicWeapon> getRods(){
        return Database.rods;
    }
    public static ArrayList<DefenseSet> getHelmets(){
        return Database.helmets;
    }
    public static ArrayList<DefenseSet> getArmors(){
        return Database.armors;
    }
    public static ArrayList<DefenseSet> getLegs(){
        return Database.legs;
    }
    public static ArrayList<DefenseSet> getBoots(){
        return Database.boots;
    }
    public static ArrayList<RingAmulet> getAmulets(){
        return Database.amulets;
    }
    public static ArrayList<RingAmulet> getRings(){
        return Database.rings;
    }
    public static ArrayList<Ammo> getBolts(){
        return Database.bolts;
    }
    public static ArrayList<Ammo> getArrows(){
        return Database.arrows;
    }
}

