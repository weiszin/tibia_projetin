/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package BackEnd;

import Classes.*;
import Enums.ProtectionNature;
import Enums.Equipments;
import Enums.Skill;
import Enums.Vocation;
import java.awt.Image;
import java.awt.image.BufferedImage;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Scanner;
import javax.imageio.ImageIO;
import javax.swing.ImageIcon;

/**
 *
 * @author Weiss
 */
public class DatabaseAux {

    public DatabaseAux(){}
    
    private static Skill getSkillByName(String name){
        if("SwordFighting".equals(name)){
            return Skill.SWORD_FIGHTING;
        }
        if("AxeFighting".equals(name)){
            return Skill.AXE_FIGHTING;
        }
        if("ClubFighting".equals(name)){
            return Skill.CLUB_FIGHTING;
        }
        if("DistanceFighting".equals(name)){
            return Skill.DISTANCE_FIGHTING;
        }
        if("FistFighting".equals(name)){
            return Skill.FIST_FIGHTING;
        }
        if("Fishing".equals(name)){
            return Skill.FISHING;
        }
        if("Shielding".equals(name)){
            return Skill.SHIELDING;
        }
        if("MagicLevel".equals(name)){
            return Skill.MAGIC_LEVEL;
        }
        if("CriticalHitChance".equals(name)){
            return Skill.CRITICAL_HIT_CHANCE;
        }
        if("CriticalExtraDamage".equals(name)){
            return Skill.CRITICAL_EXTRA_DAMAGE;
        }
        if("Speed".equals(name)){
            return Skill.SPEED;
        }
        return null;
    }
    private static ProtectionNature getProtectionNatureByName(String name){
        if("Earth".equals(name)){
            return ProtectionNature.EARTH;
        }
        if("Ice".equals(name)){
            return ProtectionNature.ICE;
        }
        if("Fire".equals(name)){
            return ProtectionNature.FIRE;
        }
        if("Physical".equals(name)){
            return ProtectionNature.PHYSICAL;
        }
        if("Death".equals(name)){
            return ProtectionNature.DEATH;
        }
        if("Holy".equals(name)){
            return ProtectionNature.HOLY; 
        }
        if("Energy".equals(name)){
            return ProtectionNature.ENERGY;
        }
        if("Drowning".equals(name)){
            return ProtectionNature.DROWNING;
        }
        if("FireField".equals(name)){
            return ProtectionNature.FIRE_FIELD;
        }
        if("ItemLost".equals(name)){
            return ProtectionNature.ITEM_LOSS;
        }
        if("ManaDrain".equals(name)){
            return ProtectionNature.MANA_DRAIN;
        }
        if("LifeDrain".equals(name)){
            return ProtectionNature.LIFE_DRAIN;
        }
        return null;
    }
    private static Vocation getVocationByName(String name){
        if("Sorcerers".equals(name)){
            return Vocation.SORCERER;
        }
        if("Druids".equals(name)){
            return Vocation.DRUID;
        }
        if("Knights".equals(name)){
            return Vocation.KNIGHT;
        }
        if("Paladins".equals(name)){
            return Vocation.PALADIN;
        }
        return null;
    }
    
    private static Shield mountShield(String[] datas, Equipments typeWeapon) throws IOException{
        Shield shield = new Shield();
        
        int pos = 0;
        
        shield.setName(datas[pos].replace("_", " "));
        String path = "";
        switch(typeWeapon){
            case SHIELD:
                path = "Imagens\\Shields\\Shields\\" + datas[0] + ".gif";
                break;
            case SPELLBOOK:
                path = "Imagens\\Shields\\Spellbooks\\" + datas[0] + ".gif";
                break;
        }
        shield.setPath(path);
        
//        BufferedImage image = ImageIO.read(new File(shield.getPath()));
//        Image shieldImage = image.getScaledInstance(45, 45, Image.SCALE_SMOOTH);
        shield.setImage(new ImageIcon(shield.getPath()));
        pos++;
        
        shield.setDefense(Integer.parseInt(datas[pos]));
        pos++;
        
        shield.setWeight(Double.parseDouble(datas[pos]));
        pos++;
        
        shield.setSlots(Integer.parseInt(datas[pos]));
        pos++;
        
        if("null".equals(datas[pos])){
            shield.setSkillBonus(null);
            pos++;
        } else {
            int qtd = Integer.parseInt(datas[pos]);
            pos++;
            SkillBonus sk = new SkillBonus();
            for(int i = 0; i < qtd; i++){
                Skill skillName = getSkillByName(datas[pos + 2 * i]);
                int value = Integer.parseInt(datas[pos + 2 * i + 1]);
                sk.addSkill(skillName, value);
            }
            pos += 2 * qtd;
            shield.setSkillBonus(sk);
        }
        
        if("null".equals(datas[pos])){
            shield.setProtection(null);
            pos++;
        } else {
            int qtd = Integer.parseInt(datas[pos]);
            pos++;
            Protection protections = new Protection();
            for(int i = 0; i < qtd; i++){
                ProtectionNature en = getProtectionNatureByName(datas[pos + 2 * i]);
                int value = Integer.parseInt(datas[pos + 2 * i + 1]);
                protections.addProtection(en, value);
            }
            pos += 2 * qtd;
            shield.setProtection(protections);
        }
        if("null".equals(datas[pos])){
            shield.setLevel(0);
        } else {
            shield.setLevel(Integer.parseInt(datas[pos]));
        }
        pos++;
        
        if("null".equals(datas[pos])){
            shield.setVocation(Vocation.ALL);
        } else {
            int qtd = Integer.parseInt(datas[pos]);
            int flag = 0;
            int KNIGHT = 0;
            int PALADIN = 1;
            int SORCERER = 2;
            int DRUID = 3;
            pos++;
            for(int i = 0; i < qtd; i++){
                if(null != datas[pos + i]) switch (datas[pos + i]) {
                    case "Knights":
                        flag |= (1 << KNIGHT);
                        break;
                    case "Paladins":
                        flag |= (1 << PALADIN);
                        break;
                    case "Sorcerers":
                        flag |= (1 << SORCERER);
                        break;
                    case "Druids":
                        flag |= (1 << DRUID);
                        break;
                    default:
                        break;
                }
            }
            switch (flag) {
                case 1:
                    shield.setVocation(Vocation.KNIGHT);
                    break;
                case 2:
                    shield.setVocation(Vocation.PALADIN);
                    break;
                case 4:
                    shield.setVocation(Vocation.SORCERER);
                    break;
                case 8:
                    shield.setVocation(Vocation.DRUID);
                    break;
                case 3:
                    shield.setVocation(Vocation.KNIGHT_PALADIN);
                    break;
                case 12:
                    shield.setVocation(Vocation.SORCERER_DRUID);
                    break;
                default:
                    break;
            }
        }
        
        return shield;
    }
    private static MeleeWeapon mountMeleeWeapon(String[] datas, Equipments typeWeapon) throws IOException{
        MeleeWeapon weapon = new MeleeWeapon();
        int pos = 0;
        weapon.setName(datas[pos].replace("_", " "));
        String path = "";
        switch(typeWeapon){
            case WEAPON_SWORD:
                path = "Imagens\\Weapons\\Swords\\" + datas[pos] + ".gif";
                break;
            case WEAPON_AXE:
                path = "Imagens\\Weapons\\Axes\\" + datas[pos] + ".gif";
                break;
            case WEAPON_CLUB:
                path = "Imagens\\Weapons\\Clubs\\" + datas[pos] + ".gif";
                break;
        }
        weapon.setPath(path);
//        BufferedImage image = ImageIO.read(new File(weapon.getPath()));
//        Image weaponImage = image.getScaledInstance(45, 45, Image.SCALE_SMOOTH);
        weapon.setImage(new ImageIcon(weapon.getPath()));
        pos++;
        
        if("null".equals(datas[pos])){
            weapon.setLevel(0);
        } else {
            weapon.setLevel(Integer.parseInt(datas[pos]));
        }
        pos++;
        
        if("null".equals(datas[pos])){
            weapon.setVocation(Vocation.ALL);
        } else {
            weapon.setVocation(Vocation.KNIGHT);
        }
        pos++;
        
        weapon.setAttack(Integer.parseInt(datas[pos]));
        pos++;
        
        if("null".equals(datas[pos])){
            weapon.setElementAttacks(null);
            pos++;
        } else {
            int qtd = Integer.parseInt(datas[pos]);
            pos++;
            ElementAttack ea = new ElementAttack();
            for(int i = 0; i < qtd; i++){
                ProtectionNature en = getProtectionNatureByName(datas[pos + 2 * i]);
                int value = Integer.parseInt(datas[pos + 2 * i + 1]);
                ea.addElementAttack(en, value);
            }
            pos += 2 * qtd;
            weapon.setElementAttacks(ea);
        }
        
        weapon.setDefense(Integer.parseInt(datas[pos]));
        pos++;
        
        if("null".equals(datas[pos])){
            weapon.setDefenseModifier(0);
        } else {
            weapon.setDefenseModifier(Integer.parseInt(datas[pos]));
        }
        pos++;
        
        weapon.setSlots(Integer.parseInt(datas[pos]));
        pos++;
        
        if("null".equals(datas[pos])){
            weapon.setSkillBonus(null);
            pos++;
        } else {
            int qtd = Integer.parseInt(datas[pos]);
            pos++;
            SkillBonus sb = new SkillBonus();
            for(int i = 0; i < qtd; i++){
                Skill sk = getSkillByName(datas[pos + 2 * i]);
                int value = Integer.parseInt(datas[pos + 2 * i + 1]);
                sb.addSkill(sk, value);
            }
            pos += 2 * qtd;
            weapon.setSkillBonus(sb);
        }
        weapon.setHands(Integer.parseInt(datas[pos]));
        pos++;
        
        weapon.setWeight(Double.parseDouble(datas[pos]));
        
        return weapon;
    }
    private static DistanceWeapon mountDistanceWeapon(String[] datas, Equipments typeWeapon){
        DistanceWeapon distanceWeapon = new DistanceWeapon();
        
        int pos = 0;
        distanceWeapon.setName(datas[pos].replace("_", " "));
        String path = "";
        switch(typeWeapon){
            case WEAPON_THROWABLE:
                path = "Imagens\\Weapons\\Throwables\\" + datas[pos] + ".gif";
                break;
            case WEAPON_BOW:
                path = "Imagens\\Weapons\\Bows\\" + datas[pos] + ".gif";
                break;
            case WEAPON_CROSSBOW:
                path = "Imagens\\Weapons\\Crossbows\\" + datas[pos] + ".gif";
                break;
        }
        distanceWeapon.setPath(path);
        distanceWeapon.setImage(new ImageIcon(distanceWeapon.getPath()));
        pos++;
        
        distanceWeapon.setLevel(Integer.parseInt(datas[pos]));
        pos++;
        
        if("null".equals(datas[pos])){
            distanceWeapon.setAttack(0);
        } else {
            distanceWeapon.setAttack(Integer.parseInt(datas[pos]));
        }
        pos++;        
        
        if("null".equals(datas[pos])){
            distanceWeapon.setElementAttacks(null);
            pos++;
        } else {
            int qtd = Integer.parseInt(datas[pos]);
            pos++;
            
            ElementAttack ea = new ElementAttack();
            for(int i = 0; i < qtd; i++){
                ProtectionNature en = getProtectionNatureByName(datas[pos + 2 * i]);
                int value = Integer.parseInt(datas[pos + 2 * i + 1]);
                ea.addElementAttack(en, value);
            }
            distanceWeapon.setElementAttacks(ea);
            pos += 2 * qtd;
        }
        
        if("null".equals(datas[pos])){
            distanceWeapon.setAttackModifier(0);
        } else {
            distanceWeapon.setAttackModifier(Integer.parseInt(datas[pos]));
        }
        pos++;
        
        if("null".equals(datas[pos])){
            distanceWeapon.setSkillBonus(null);
            pos++;
        } else {
            int qtd = Integer.parseInt(datas[pos]);
            pos++;
            SkillBonus sb = new SkillBonus();
            for(int i = 0; i < qtd; i++){
                Skill sk = getSkillByName(datas[pos + 2 * i]);
                int value = Integer.parseInt(datas[pos + 2 * i + 1]);
                sb.addSkill(sk, value);
            }
            distanceWeapon.setSkillBonus(sb);
            pos += 2 * qtd;
        }
        
        if("null".equals(datas[pos])){
            distanceWeapon.setHitModifier(0);
        } else {
            distanceWeapon.setHitModifier(Integer.parseInt(datas[pos]));
        }
        pos++;
        
        if("null".equals(datas[pos])){
            distanceWeapon.setHands(0);
        } else {
            distanceWeapon.setHands(Integer.parseInt(datas[pos]));
        }        
        pos++;
        
        if("null".equals(datas[pos])){
            distanceWeapon.setWeight(0.0);
        } else {
            distanceWeapon.setWeight(Double.parseDouble(datas[pos]));
        }
        pos++;
        
        distanceWeapon.setSlots(Integer.parseInt(datas[pos]));
        pos++;
        
        if("null".equals(datas[pos])){
            distanceWeapon.setAttackRange(0);
        } else {
            distanceWeapon.setAttackRange(Integer.parseInt(datas[pos]));
        }
        
        return distanceWeapon;
    }
    private static MagicWeapon mountMagicWeapon(String[] datas, Equipments typeWeapon){
        MagicWeapon magicWeapon = new MagicWeapon();
        
        int pos = 0;
        
        magicWeapon.setName(datas[pos].replace("_", " "));
        String path = "";
        switch(typeWeapon){
            case WEAPON_WAND:
                path = "Imagens\\Weapons\\Wands\\" + datas[pos] + ".gif";
                break;
            case WEAPON_ROD:
                path = "Imagens\\Weapons\\Rods\\" + datas[pos] + ".gif";
                break;
        }
        magicWeapon.setPath(path);
        magicWeapon.setImage(new ImageIcon(magicWeapon.getPath()));
        pos++;
        
        magicWeapon.setLevel(Integer.parseInt(datas[pos]));
        pos++;
        
        ElementAttack ea = new ElementAttack();
        ProtectionNature en = getProtectionNatureByName(datas[pos]);
        int value = 0;
        if("null".equals(datas[pos + 1])){
            value = 0;
        } else {
            value = Integer.parseInt(datas[pos + 1]);
        }
        ea.addElementAttack(en, value);
        magicWeapon.setElementAttacks(ea);
        pos += 2;
        
        if("null".equals(datas[pos])){
            magicWeapon.setManaAttack(0);
        } else {
            magicWeapon.setManaAttack(Integer.parseInt(datas[pos]));
        }
        pos++;
        
        magicWeapon.setSlots(Integer.parseInt(datas[pos]));
        pos++;
        
        if("null".equals(datas[pos])){
            magicWeapon.setSkillBonus(null);
            pos++;
        } else {
            int qtd = Integer.parseInt(datas[pos]);
            pos++;
            SkillBonus sb = new SkillBonus();
            for(int i = 0; i < qtd; i++){
                Skill sk = getSkillByName(datas[pos + 2 * i]);
                value = Integer.parseInt(datas[pos + 2 * i + 1]);
                sb.addSkill(sk, value);
            }
            magicWeapon.setSkillBonus(sb);
            pos += 2 * qtd;            
        }
        
        if("null".equals(datas[pos])){
            magicWeapon.setProtections(null);
            pos++;
        } else {
            int qtd = Integer.parseInt(datas[pos]);
            pos++;
            Protection prot = new Protection();
            for(int i = 0; i < qtd; i++){
                en = getProtectionNatureByName(datas[pos + 2 * i]);
                value = Integer.parseInt(datas[pos + 2 * i + 1]);
                prot.addProtection(en, value);
            }
            magicWeapon.setProtections(prot);
            pos += 2 * qtd;
        }
        
        magicWeapon.setWeight(Double.parseDouble(datas[pos]));
        pos++;
        
        magicWeapon.setVocation(getVocationByName(datas[pos]));
        
        return magicWeapon;
    }
    private static DefenseSet mountDefenseSet(String[] datas, Equipments typeWeapon){
        DefenseSet defenseSet = new DefenseSet();
        
        int pos = 0;
        
        defenseSet.setName(datas[pos].replace("_", " "));
        String path = "";
        switch(typeWeapon){
            case HELMET:
                path = "Imagens\\Helmets\\" + datas[0] + ".gif";
                break;
            case ARMOR:
                path = "Imagens\\Armors\\" + datas[0] + ".gif";
                break;
            case LEGS:
                path = "Imagens\\Legs\\" + datas[0] + ".gif";
                break;
            case BOOTS:
                path = "Imagens\\Boots\\" + datas[0] + ".gif";
                break;
        }
        defenseSet.setPath(path);
        
//        BufferedImage image = ImageIO.read(new File(shield.getPath()));
//        Image shieldImage = image.getScaledInstance(45, 45, Image.SCALE_SMOOTH);
        defenseSet.setImage(new ImageIcon(defenseSet.getPath()));
        pos++;
        
        if("null".equals(datas[pos])){
            defenseSet.setDefense(0);
        } else {
            defenseSet.setDefense(Integer.parseInt(datas[pos]));
        }
        pos++;
        
        defenseSet.setWeight(Double.parseDouble(datas[pos]));
        pos++;
        
        defenseSet.setSlots(Integer.parseInt(datas[pos]));
        pos++;
        
        if("null".equals(datas[pos])){
            defenseSet.setSkillBonus(null);
            pos++;
        } else {
            int qtd = Integer.parseInt(datas[pos]);
            pos++;
            SkillBonus sk = new SkillBonus();
            for(int i = 0; i < qtd; i++){
                Skill skillName = getSkillByName(datas[pos + 2 * i]);
                int value = Integer.parseInt(datas[pos + 2 * i + 1]);
                sk.addSkill(skillName, value);
            }
            pos += 2 * qtd;
            defenseSet.setSkillBonus(sk);
        }
        
        if("null".equals(datas[pos])){
            defenseSet.setProtection(null);
            pos++;
        } else {
            int qtd = Integer.parseInt(datas[pos]);
            pos++;
            Protection protections = new Protection();
            for(int i = 0; i < qtd; i++){
                ProtectionNature en = getProtectionNatureByName(datas[pos + 2 * i]);
                int value = Integer.parseInt(datas[pos + 2 * i + 1]);
                protections.addProtection(en, value);
            }
            pos += 2 * qtd;
            defenseSet.setProtection(protections);
        }
        if("null".equals(datas[pos])){
            defenseSet.setLevel(0);
        } else {
            defenseSet.setLevel(Integer.parseInt(datas[pos]));
        }
        pos++;
        
        if("null".equals(datas[pos])){
            defenseSet.setVocation(Vocation.ALL);
        } else {
            int qtd = Integer.parseInt(datas[pos]);
            int flag = 0;
            int KNIGHT = 0;
            int PALADIN = 1;
            int SORCERER = 2;
            int DRUID = 3;
            pos++;
            for(int i = 0; i < qtd; i++){
                if(null != datas[pos + i]) switch (datas[pos + i]) {
                    case "Knights":
                        flag |= (1 << KNIGHT);
                        break;
                    case "Paladins":
                        flag |= (1 << PALADIN);
                        break;
                    case "Sorcerers":
                        flag |= (1 << SORCERER);
                        break;
                    case "Druids":
                        flag |= (1 << DRUID);
                        break;
                    default:
                        break;
                }
            }
            switch (flag) {
                case 1:
                    defenseSet.setVocation(Vocation.KNIGHT);
                    break;
                case 2:
                    defenseSet.setVocation(Vocation.PALADIN);
                    break;
                case 4:
                    defenseSet.setVocation(Vocation.SORCERER);
                    break;
                case 8:
                    defenseSet.setVocation(Vocation.DRUID);
                    break;
                case 3:
                    defenseSet.setVocation(Vocation.KNIGHT_PALADIN);
                    break;
                case 12:
                    defenseSet.setVocation(Vocation.SORCERER_DRUID);
                    break;
                default:
                    break;
            }
        }
//        System.out.println(defenseSet.getName());
//        System.out.println(defenseSet.getSkillBonus());
        return defenseSet;
    }
    private static RingAmulet mountRingAmulet(String[] datas, Equipments typeWeapon) throws IOException{
        RingAmulet ringAmulet = new RingAmulet();
        int pos = 0;

        ringAmulet.setName(datas[pos].replace("_", " "));
        String path = "";
        switch(typeWeapon){
            case AMULET:
                path = "Imagens\\Amulets\\" + datas[0] + ".gif";
                break;
            case RING:
                path = "Imagens\\Rings\\" + datas[0] + ".gif";
                break;
        }
        ringAmulet.setPath(path);
        File file = new File(ringAmulet.getPath());
        BufferedImage img = ImageIO.read(file);
        ringAmulet.setImage(new ImageIcon(img));
        pos++;
        
        if("null".equals(datas[pos])){
            ringAmulet.setDefense(0);
        } else {
            ringAmulet.setDefense(Integer.parseInt(datas[pos]));
        }
        pos++;
        
        if("null".equals(datas[pos])){
            ringAmulet.setCharge(-1);
        } else {
            ringAmulet.setCharge(Integer.parseInt(datas[pos]));
        }
        pos++;
        
        if("null".equals(datas[pos])){
            ringAmulet.setLevel(0);
        } else {
            ringAmulet.setLevel(Integer.parseInt(datas[pos]));
        }
        pos++;
        
        ringAmulet.setWeight(Double.parseDouble(datas[pos]));
        pos++;
        
        if("null".equals(datas[pos])){
            ringAmulet.setSkillBonus(null);
            pos++;
        } else {
            int qtd = Integer.parseInt(datas[pos]);
            pos++;
            SkillBonus sb = new SkillBonus();
            for(int i = 0; i < qtd; i++){
                Skill sk = getSkillByName(datas[pos + 2 * i]);
                int value = Integer.parseInt(datas[pos + 2 * i + 1]);
                
                sb.addSkill(sk, value);
            }
            ringAmulet.setSkillBonus(sb);
            pos += 2 * qtd;
        }
        
        if("null".equals(datas[pos])){
            ringAmulet.setProtection(null);
        } else {
            int qtd = Integer.parseInt(datas[pos]);
            pos++;
            Protection sb = new Protection();
            for(int i = 0; i < qtd; i++){
                ProtectionNature sk = getProtectionNatureByName(datas[pos + 2 * i]);
                
                int value = Integer.parseInt(datas[pos + 2 * i + 1]);
                
                sb.addProtection(sk, value);
            }
            ringAmulet.setProtection(sb);
        }
        
        return ringAmulet;
    }
    private static Ammo mountAmmo(String[] datas, Equipments typeWeapon) throws IOException{
        Ammo ammo = new Ammo();
        
        int pos = 0;
        
        ammo.setName(datas[pos].replace("_", " "));
        String path = "";
        switch(typeWeapon){
            case AMMO_BOLT:
                path = "Imagens\\Ammos\\Bolts\\" + datas[0] + ".gif";
                break;
            case AMMO_ARROW:
                path = "Imagens\\Ammos\\Arrows\\" + datas[0] + ".gif";
                break;
        }
        ammo.setPath(path);
        File file = new File(ammo.getPath());
        BufferedImage bf = ImageIO.read(file);
        ammo.setImage(new ImageIcon(bf));
        pos++;
        
        if("null".equals(datas[pos])){
            ammo.setLevel(0);
        } else {
            ammo.setLevel(Integer.parseInt(datas[pos]));
        }
        pos++;
        
        ammo.setAttack(Integer.parseInt(datas[pos]));
        pos++;
        
        if("null".equals(datas[pos])){
            ammo.setElementAttack(null);
            pos++;
        } else {
            int qtd = Integer.parseInt(datas[pos]);
            pos++;
            ElementAttack ea = new ElementAttack();
            for(int i = 0; i < qtd; i++){
                ProtectionNature pn = getProtectionNatureByName(datas[pos + 2 * i]);
                int value = Integer.parseInt(datas[pos + 2 * i + 1]);
                ea.addElementAttack(pn, value);
            }
            ammo.setElementAttack(ea);
            pos += 2 * qtd;
        }
        
        ammo.setWeight(Double.parseDouble(datas[pos]));
        
        return ammo;
    }
    
    public static ArrayList<Shield> loadShields(String path, Equipments typeWeapon) throws FileNotFoundException, IOException{
        ArrayList<Shield> listaShields = new ArrayList<>();
        File file = new File(path);
        Scanner sc = new Scanner(file);
        
        while(sc.hasNextLine()){
            String line = sc.nextLine();
            String[] datas = line.split(" ");
            Shield shield = mountShield(datas, typeWeapon);
            listaShields.add(shield);
        }
        
        return listaShields;
   }
    public static ArrayList<MeleeWeapon> loadMeleeWeapons(String path, Equipments typeWeapon) throws FileNotFoundException, IOException{
        ArrayList<MeleeWeapon> listaMeleeWeapons = new ArrayList<>();
        
        File file = new File(path);
        Scanner sc = new Scanner(file);
        
        while(sc.hasNextLine()){
            String line = sc.nextLine();
            String[] datas = line.split(" ");
            MeleeWeapon weapon = mountMeleeWeapon(datas, typeWeapon);
            listaMeleeWeapons.add(weapon);
        }
        
        return listaMeleeWeapons;
    }
    public static ArrayList<DistanceWeapon> loadDistanceWeapons(String path, Equipments typeWeapon) throws FileNotFoundException, IOException{
        ArrayList<DistanceWeapon> listaDistanceWeapons = new ArrayList<>();
        
        File file = new File(path);
        Scanner sc = new Scanner(file);
        
        while(sc.hasNextLine()){
            String line = sc.nextLine();
            String[] datas = line.split(" ");
            DistanceWeapon weapon = mountDistanceWeapon(datas, typeWeapon);
            listaDistanceWeapons.add(weapon);
        }
        
        return listaDistanceWeapons;    
    }
    public static ArrayList<MagicWeapon> loadMagicWeapons(String path, Equipments typeWeapon) throws FileNotFoundException{
        ArrayList<MagicWeapon> listaMagicWeapons = new ArrayList<>();
        
        File file = new File(path);
        Scanner sc = new Scanner(file);
        
        while(sc.hasNextLine()){
            String line = sc.nextLine();
            String[] datas = line.split(" ");
            MagicWeapon weapon = mountMagicWeapon(datas, typeWeapon);
            listaMagicWeapons.add(weapon);
        }
        
        return listaMagicWeapons;
    }
    public static ArrayList<DefenseSet> loadDefenseSet(String path, Equipments typeWeapon) throws FileNotFoundException{
        ArrayList<DefenseSet> listaDefenseSet = new ArrayList<>();
        File file = new File(path);
        Scanner sc = new Scanner(file);
        
        while(sc.hasNextLine()){
            String line = sc.nextLine();
            String[] datas = line.split(" ");
            DefenseSet defenseSet = mountDefenseSet(datas, typeWeapon);
            listaDefenseSet.add(defenseSet);
        }
        
        return listaDefenseSet;
    }
    public static ArrayList<RingAmulet> loadRingAmulet(String path, Equipments typeWeapon) throws FileNotFoundException, UnsupportedEncodingException, IOException{
        ArrayList<RingAmulet> listaRingAmulets = new ArrayList<>();
        
        File file = new File(path);
//        Scanner sc = new Scanner(file);
        FileInputStream fs1 = new FileInputStream(file);
        BufferedReader br1 = new BufferedReader(new InputStreamReader(fs1, "UTF-8"));
        String line;
        while((line = br1.readLine()) != null){
//            System.out.println(line);
            String[] datas = line.split(" ");
            RingAmulet ringAmulet = mountRingAmulet(datas, typeWeapon);
            listaRingAmulets.add(ringAmulet);
        }
       
        return listaRingAmulets;
    }
    public static ArrayList<Ammo> loadAmmo(String path, Equipments typeWeapon) throws FileNotFoundException, UnsupportedEncodingException, IOException{
        ArrayList<Ammo> listaAmmos = new ArrayList<>();
        
        File file = new File(path);
//        Scanner sc = new Scanner(file);
        FileInputStream fs1 = new FileInputStream(file);
        BufferedReader br1 = new BufferedReader(new InputStreamReader(fs1, "UTF-8"));
        String line;
        while((line = br1.readLine()) != null){
            String[] datas = line.split(" ");
            Ammo ammo = mountAmmo(datas, typeWeapon);
            listaAmmos.add(ammo);
        }
        
        return listaAmmos;
    }
}
