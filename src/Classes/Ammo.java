/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Classes;

import Enums.Vocation;
import javax.swing.ImageIcon;

/**
 *
 * @author Ivan
 */
public class Ammo {
    private ImageIcon image;
    private String path;
    private String name;
    private double weight;
    private int level;
    private int attack;
    private ElementAttack elementAttack;

    public ImageIcon getImage() {
        return image;
    }

    public void setImage(ImageIcon image) {
        this.image = image;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getWeight() {
        return weight;
    }

    public void setWeight(double weight) {
        this.weight = weight;
    }

    public int getLevel() {
        return level;
    }

    public void setLevel(int level) {
        this.level = level;
    }

    public ElementAttack getElementAttack() {
        return elementAttack;
    }

    public void setElementAttack(ElementAttack elementAttack) {
        this.elementAttack = elementAttack;
    }

    public int getAttack() {
        return attack;
    }

    public void setAttack(int attack) {
        this.attack = attack;
    }
}
