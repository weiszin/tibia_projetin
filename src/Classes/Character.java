/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Classes;

import Enums.Skill;
import Enums.Vocation;
import java.awt.Image;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import javax.imageio.ImageIO;
import javax.swing.ImageIcon;

/**
 *
 * @author Weiss
 */
public class Character {
    private static final Character INSTANCE = new Character();
    private Vocation vocation;
    private DefenseSet helmet;
    private DefenseSet armor;
    private DefenseSet legs;
    private DefenseSet boots;
    private MeleeWeapon meleeWeapon;
    private MagicWeapon magicWeapon;
    private DistanceWeapon distanceWeapon;
    private Shield shield;
    private RingAmulet ring;
    private RingAmulet amulet;
    private Ammo ammo;
    private int level;
    private int magicLevel;
    private int axeSkill;
    private int swordSkill;
    private int clubSkill;
    private int distanceSkill;
    private int shieldSkill;
    private int speedBonus;

    private Character(){
        helmet = null;
        armor = null;
        legs = null;
        boots = null;
        meleeWeapon = null;
        magicWeapon = null;
        distanceWeapon = null;
        shield = null;
        ring = null;
        amulet = null;
        ammo = null;
        level = magicLevel = swordSkill = axeSkill = clubSkill = distanceSkill = shieldSkill = speedBonus = 0;
    }
    
    public static Character getInstance(){
        return INSTANCE;
    }

    public Vocation getVocation() {
        return vocation;
    }

    public void setVocation(Vocation vocation) {
        this.vocation = vocation;
    }

    public DefenseSet getHelmet() {
        return helmet;
    }

    public void setHelmet(DefenseSet helmet) {
        if(this.helmet != null) updateSkillBonusMinus(this.helmet.getSkillBonus());
        if(helmet != null) updateSkillBonusPlus(helmet.getSkillBonus());
        this.helmet = helmet;
    }

    public DefenseSet getArmor() {
        return armor;
    }

    public void setArmor(DefenseSet armor) {
        if(this.armor != null) updateSkillBonusMinus(this.armor.getSkillBonus());
        if(armor != null) updateSkillBonusPlus(armor.getSkillBonus());
        this.armor = armor;
    }

    public DefenseSet getLegs() {
        return legs;
    }

    public void setLegs(DefenseSet legs) {
        if(this.legs != null) updateSkillBonusMinus(this.legs.getSkillBonus());
        if(legs != null) updateSkillBonusPlus(legs.getSkillBonus());
        this.legs = legs;
    }

    public DefenseSet getBoots() {
        return boots;
    }

    public void setBoots(DefenseSet boots) {
        if(this.boots != null) updateSkillBonusMinus(this.boots.getSkillBonus());
        if(boots != null) updateSkillBonusPlus(boots.getSkillBonus());
        this.boots = boots;
    }

    public MeleeWeapon getMeleeWeapon() {
        return meleeWeapon;
    }

    public void setMeleeWeapon(MeleeWeapon meleeWeapon) {
        if(this.meleeWeapon != null) updateSkillBonusMinus(this.meleeWeapon.getSkillBonus());
        if(meleeWeapon != null) updateSkillBonusPlus(meleeWeapon.getSkillBonus());
        
        this.meleeWeapon = meleeWeapon;
        this.magicWeapon = null;
        this.distanceWeapon = null;
    }

    public MagicWeapon getMagicWeapon() {
        return magicWeapon;
    }

    public void setMagicWeapon(MagicWeapon magicWeapon) {
        if(this.magicWeapon != null) updateSkillBonusMinus(this.magicWeapon.getSkillBonus());
        if(magicWeapon != null) updateSkillBonusPlus(magicWeapon.getSkillBonus());
        
        this.magicWeapon = magicWeapon;
        this.meleeWeapon = null;
        this.distanceWeapon = null;
    }

    public DistanceWeapon getDistanceWeapon() {
        return distanceWeapon;
    }

    public void setDistanceWeapon(DistanceWeapon distanceWeapon) {
        if(this.distanceWeapon != null) updateSkillBonusMinus(this.distanceWeapon.getSkillBonus());
        if(distanceWeapon != null) updateSkillBonusPlus(distanceWeapon.getSkillBonus());
        
        this.distanceWeapon = distanceWeapon;
        this.magicWeapon = null;
        this.meleeWeapon = null;
    }

    public Shield getShield() {
        return shield;
    }

    public void setShield(Shield shield) {
        if(this.shield != null) updateSkillBonusMinus(this.shield.getSkillBonus());
        if(shield != null) updateSkillBonusPlus(shield.getSkillBonus());
        
        this.shield = shield;
    }

    public RingAmulet getRing() {
        return ring;
    }

    public void setRing(RingAmulet ring) {
        if(this.ring != null) updateSkillBonusMinus(this.ring.getSkillBonus());
        if(ring != null) updateSkillBonusPlus(ring.getSkillBonus());
        
        this.ring = ring;
    }

    public RingAmulet getAmulet() {
        return amulet;
    }

    public void setAmulet(RingAmulet amulet) {
        if(this.amulet != null) updateSkillBonusMinus(this.amulet.getSkillBonus());
        if(amulet != null) updateSkillBonusPlus(amulet.getSkillBonus());
        
        this.amulet = amulet;
    }

    public Ammo getAmmo() {
        return ammo;
    }

    public void setAmmo(Ammo ammo) {
        this.ammo = ammo;
    }

    public int getLevel() {
        return level;
    }

    public void setLevel(int level) {
        this.level = level;
    }

    public int getAxeSkill() {
        return axeSkill;
    }

    public void setAxeSkill(int axeSkill) {
        this.axeSkill = axeSkill;
    }

    public int getSwordSkill() {
        return swordSkill;
    }

    public void setSwordSkill(int swordSkill) {
        this.swordSkill = swordSkill;
    }

    public int getClubSkill() {
        return clubSkill;
    }

    public void setClubSkill(int clubSkill) {
        this.clubSkill = clubSkill;
    }

    public int getDistanceSkill() {
        return distanceSkill;
    }

    public void setDistanceSkill(int distanceSkill) {
        this.distanceSkill = distanceSkill;
    }

    public int getShieldSkill() {
        return shieldSkill;
    }

    public void setShieldSkill(int shieldSkill) {
        this.shieldSkill = shieldSkill;
    }    

    public int getMagicLevel() {
        return magicLevel;
    }

    public void setMagicLevel(int magicLevel) {
        this.magicLevel = magicLevel;
    }

    public int getSpeedBonus() {
        return speedBonus;
    }

    public void setSpeedBonus(int speedBonus) {
        this.speedBonus = speedBonus;
    }
    
    private void updateSkillBonusPlus(SkillBonus sb){
        if(sb == null) return;
        for(SkillBonus.Pair sbp : sb.getSkills()){
            Skill sk = sbp.getTipoSkill();
            int value = sbp.getValue();
            switch(sk){
                case SPEED:
                    this.speedBonus += value;
                    break;
            }
        }
    }
    private void updateSkillBonusMinus(SkillBonus sb){
        if(sb == null) return;
        for(SkillBonus.Pair sbp : sb.getSkills()){
            Skill sk = sbp.getTipoSkill();
            int value = sbp.getValue();
            switch(sk){
                case SPEED:
                    this.speedBonus -= value;
                    break;
            }
        }
    }
}
