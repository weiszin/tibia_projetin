/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Classes;

import javax.swing.ImageIcon;

/**
 *
 * @author Ivan
 */
public class DistanceWeapon {
    private String name;
    private String path;
    private ImageIcon image;
    private int level;
    private int attack;
    private ElementAttack elementAttacks;
    private int attackModifier;
    private int slots;
    private SkillBonus skillBonus;
    private int hands;
    private double weight;
    private int hitModifier;
    private int attackRange;

    public DistanceWeapon(){}
    
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public ImageIcon getImage() {
        return image;
    }

    public void setImage(ImageIcon image) {
        this.image = image;
    }

    public int getLevel() {
        return level;
    }

    public void setLevel(int level) {
        this.level = level;
    }

    public int getAttack() {
        return attack;
    }

    public void setAttack(int attack) {
        this.attack = attack;
    }

    public ElementAttack getElementAttacks() {
        return elementAttacks;
    }

    public void setElementAttacks(ElementAttack elementAttacks) {
        this.elementAttacks = elementAttacks;
    }

    public int getAttackModifier() {
        return attackModifier;
    }

    public void setAttackModifier(int attackModifier) {
        this.attackModifier = attackModifier;
    }

    public int getSlots() {
        return slots;
    }

    public void setSlots(int slots) {
        this.slots = slots;
    }

    public SkillBonus getSkillBonus() {
        return skillBonus;
    }

    public void setSkillBonus(SkillBonus skillBonus) {
        this.skillBonus = skillBonus;
    }

    public int getHands() {
        return hands;
    }

    public void setHands(int hands) {
        this.hands = hands;
    }

    public double getWeight() {
        return weight;
    }

    public void setWeight(double weight) {
        this.weight = weight;
    }

    public int getHitModifier() {
        return hitModifier;
    }

    public void setHitModifier(int hitModifier) {
        this.hitModifier = hitModifier;
    }

    public int getAttackRange() {
        return attackRange;
    }

    public void setAttackRange(int attackRange) {
        this.attackRange = attackRange;
    }
}
