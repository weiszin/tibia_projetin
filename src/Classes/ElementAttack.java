/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Classes;

import Enums.ProtectionNature;
import java.util.ArrayList;

/**
 *
 * @author Weiss
 */
public class ElementAttack {
    public class Pair {
        private ProtectionNature en;
        private int value;
        
        public Pair(ProtectionNature en, int value){
            this.en = en;
            this.value = value;
        }

        public int getValue() {
            return value;
        }

        public void setValue(int value) {
            this.value = value;
        }

        public ProtectionNature getEn() {
            return en;
        }

        public void setPn(ProtectionNature en) {
            this.en = en;
        }
        
        
    }
    
    private ArrayList<Pair> elementAttacks;

    public ArrayList<Pair> getElementAttacks() {
        return elementAttacks;
    }

    public void setElementAttacks(ArrayList<Pair> elementAttacks) {
        this.elementAttacks = elementAttacks;
    }
    
    public void addElementAttack(ProtectionNature en, int value){
        this.elementAttacks.add(new Pair(en, value));
    }
    
    public ElementAttack(){
        this.elementAttacks = new ArrayList<>();
    }
    
    public ElementAttack(ArrayList<Pair> elementAttacks){
        this.elementAttacks = elementAttacks;
    }
}
