/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Classes;

import Enums.Vocation;
import javax.swing.ImageIcon;

/**
 *
 * @author Ivan
 */
public class MagicWeapon {
    private String name;
    private String path;
    private ImageIcon image;
    private int level;
    private Vocation vocation;
    private ElementAttack elementAttacks;
    private int manaAttack;
    private int slots;
    private SkillBonus skillBonus;
    private Protection protections;
    private double weight;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public ImageIcon getImage() {
        return image;
    }

    public void setImage(ImageIcon image) {
        this.image = image;
    }

    public int getLevel() {
        return level;
    }

    public void setLevel(int level) {
        this.level = level;
    }

    public Vocation getVocation() {
        return vocation;
    }

    public void setVocation(Vocation vocation) {
        this.vocation = vocation;
    }

    public ElementAttack getElementAttacks() {
        return elementAttacks;
    }

    public void setElementAttacks(ElementAttack elementAttacks) {
        this.elementAttacks = elementAttacks;
    }

    public int getManaAttack() {
        return manaAttack;
    }

    public void setManaAttack(int manaAttack) {
        this.manaAttack = manaAttack;
    }

    public int getSlots() {
        return slots;
    }

    public void setSlots(int slots) {
        this.slots = slots;
    }

    public SkillBonus getSkillBonus() {
        return skillBonus;
    }

    public void setSkillBonus(SkillBonus skillBonus) {
        this.skillBonus = skillBonus;
    }

    public Protection getProtections() {
        return protections;
    }

    public void setProtections(Protection protections) {
        this.protections = protections;
    }

    public double getWeight() {
        return weight;
    }

    public void setWeight(double weight) {
        this.weight = weight;
    }
}
