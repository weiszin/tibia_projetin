/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Classes;

import Enums.Vocation;
import javax.swing.ImageIcon;

/**
 *
 * @author Ivan
 */
public class MeleeWeapon {
    private String name;
    private String path;
    private ImageIcon image;
    private int level;
    private Vocation vocation;
    private int attack;
    private ElementAttack elementAttacks;
    private int defense;
    private int defenseModifier;
    private int slots;
    private SkillBonus skillBonus;
    private int hands;
    private double weight;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public ImageIcon getImage() {
        return image;
    }

    public void setImage(ImageIcon image) {
        this.image = image;
    }

    public int getLevel() {
        return level;
    }

    public void setLevel(int level) {
        this.level = level;
    }

    public Vocation getVocation() {
        return vocation;
    }

    public void setVocation(Vocation vocation) {
        this.vocation = vocation;
    }

    public int getAttack() {
        return attack;
    }

    public void setAttack(int attack) {
        this.attack = attack;
    }

    public int getDefense() {
        return defense;
    }

    public void setDefense(int defense) {
        this.defense = defense;
    }

    public int getSlots() {
        return slots;
    }

    public void setSlots(int slots) {
        this.slots = slots;
    }

    public SkillBonus getSkillBonus() {
        return skillBonus;
    }

    public void setSkillBonus(SkillBonus skillBonus) {
        this.skillBonus = skillBonus;
    }

    public int getHands() {
        return hands;
    }

    public void setHands(int hands) {
        this.hands = hands;
    }

    public double getWeight() {
        return weight;
    }

    public void setWeight(double weight) {
        this.weight = weight;
    }

    public ElementAttack getElementAttacks() {
        return elementAttacks;
    }

    public void setElementAttacks(ElementAttack elementAttacks) {
        this.elementAttacks = elementAttacks;
    }

    public int getDefenseModifier() {
        return defenseModifier;
    }

    public void setDefenseModifier(int defenseModifier) {
        this.defenseModifier = defenseModifier;
    }
}
