/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Classes;

import Enums.ProtectionNature;
import java.util.ArrayList;

/**
 *
 * @author Weiss
 */
public class Protection {
    public class Pair {
        private ProtectionNature pn;
        private int value;
        
        public Pair(ProtectionNature pn, int value){
            this.pn = pn;
            this.value = value;
        }

        public int getValue() {
            return value;
        }

        public void setValue(int value) {
            this.value = value;
        }

        public ProtectionNature getPn() {
            return pn;
        }

        public void setPn(ProtectionNature pn) {
            this.pn = pn;
        }
        
        
    }
    
    private ArrayList<Pair> protections;

    public ArrayList<Pair> getProtections() {
        return protections;
    }

    public void setProtections(ArrayList<Pair> protections) {
        this.protections = protections;
    }
    
    public void addProtection(ProtectionNature en, int value){
        this.protections.add(new Pair(en, value));
    }
    
    public Protection(){
        this.protections = new ArrayList<>();
    }
    
    public Protection(ArrayList<Pair> protections){
        this.protections = protections;
    }
    
    private String getNameElementNature(ProtectionNature en){
        switch(en){
            case FIRE:
                return "Fire";
            case ENERGY:
                return "Energy";
            case PHYSICAL:
                return "Physical";
            case EARTH:
                return "Earth";
            case ICE:
                return "Ice";
            case DEATH:
                return "Death";
            case HOLY:
                return "Holy";
            case DROWNING:
                return "Drowning";
            case FIRE_FIELD:
                return "Fire Field";
            case ITEM_LOSS:
                return "Item Loss";
            case MANA_DRAIN:
                return "Mana Drain";
            case LIFE_DRAIN:
                return "Life Drain";
        }
        return "None";
    }
    
    @Override
    public String toString(){
        String ret = "";
        for(int i = 0; i < protections.size(); i++){
            if(i > 0) ret += ", ";
            String name = getNameElementNature(protections.get(i).getPn());
            int value = protections.get(i).getValue();
            ret += name + " " + (value < 0 ? "" : "+") + value + "%";
        }
        return ret;
    }
}
