/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Classes;

import Enums.Skill;
import java.util.ArrayList;

/**
 *
 * @author Weiss
 */

public class SkillBonus {
    
    public class Pair {
        private Skill tipoSkill;
        private int value;
        
        public Pair(Skill tipoSkill, int value) {
            this.tipoSkill = tipoSkill;
            this.value = value;
        }
        
        public Skill getTipoSkill() {
            return tipoSkill;
        }

        public void setTipoSkill(Skill tipoSkill) {
            this.tipoSkill = tipoSkill;
        }
        
        public void setSkill(Skill sk){
            this.tipoSkill = sk;
        }
        public void setValue(int value){
            this.value = value;
        }
        public int getValue(){
            return this.value;
        }
        
    }
    
    private ArrayList<Pair> skills = new ArrayList<>();
    
    public void addSkill(Skill sk, int value){
        this.skills.add(new Pair(sk, value));
    }
    
    public ArrayList<Pair> getSkills() {
        return skills;
    }

    public void setSkills(ArrayList<Pair> skills) {
        this.skills = skills;
    }
    
    public SkillBonus(){
        skills = new ArrayList<>();
    }
    
    private String getNameSkill(Skill sk){
        /*
        SWORD_FIGHTING,
        AXE_FIGHTING,
        CLUB_FIGHTING,
        MAGIC_LEVEL,
        SPEED,
        SHIELDING,
        FISHING,
        FISTING,
        CRITICAL_EXTRA_DAMAGE,
        CRITICAL_HIT_CHANCE
        */
        switch(sk){
            case SWORD_FIGHTING:
                return "Sword Fighting";
            case AXE_FIGHTING:
                return "Axe Fighting";
            case CLUB_FIGHTING:
                return "Club Fighting";
            case DISTANCE_FIGHTING:
                return "Distance Fighting";
            case MAGIC_LEVEL:
                return "Magic Level";
            case SPEED:
                return "Speed";
            case SHIELDING:
                return "Shielding";
            case FISHING:
                return "Fishing";
            case FIST_FIGHTING:
                return "Fisting";
            case CRITICAL_EXTRA_DAMAGE:
                return "Critical Extra Damage";
            case CRITICAL_HIT_CHANCE:
                return "Critical Hit Chance";
        }
        return "None";
    }
    
    @Override
    public String toString(){
        String ret = "";
        for(int i = 0; i < skills.size(); i++){
            if(i > 0) ret += ", ";
            String name = getNameSkill(skills.get(i).getTipoSkill());
            int value = skills.get(i).getValue();
            ret += name + " " + (value < 0 ? "" : "+") + value;
        }
        return ret;
    }
    
}
