/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Enums;

/**
 *
 * @author Weiss
 */
public enum Equipments {
    WEAPON_SWORD,
    WEAPON_AXE,
    WEAPON_CLUB,
    WEAPON_ROD,
    WEAPON_WAND,
    WEAPON_CROSSBOW,
    WEAPON_BOW,
    WEAPON_THROWABLE,
    SHIELD,
    SPELLBOOK,
    HELMET,
    ARMOR,
    LEGS,
    BOOTS,
    AMULET,
    RING,
    AMMO_ARROW,
    AMMO_BOLT,
    BACKPACK
}
