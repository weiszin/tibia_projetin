/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Enums;

/**
 *
 * @author Weiss
 */
public enum ProtectionNature {
    FIRE,
    ENERGY,
    PHYSICAL,
    EARTH,
    ICE,
    DEATH,
    HOLY,
    DROWNING,
    FIRE_FIELD,
    ITEM_LOSS,
    MANA_DRAIN,
    LIFE_DRAIN
}
