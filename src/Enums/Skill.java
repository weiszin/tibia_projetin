/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Enums;

/**
 *
 * @author Weiss
 */
public enum Skill {
    SWORD_FIGHTING,
    AXE_FIGHTING,
    CLUB_FIGHTING,
    DISTANCE_FIGHTING,
    FIST_FIGHTING,
    MAGIC_LEVEL,
    SPEED,
    SHIELDING,
    FISHING,
    CRITICAL_EXTRA_DAMAGE,
    CRITICAL_HIT_CHANCE
}
