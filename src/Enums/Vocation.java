/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Enums;

/**
 *
 * @author Weiss
 */
public enum Vocation {
    KNIGHT,
    ELITE_KNIGHT,
    PALADIN,
    ROYAL_PALADIN,
    SORCERER,
    MASTER_SORCERER,
    DRUID,
    ELDER_DRUID,
    ALL,
    NO_VOCATION,
    KNIGHT_PALADIN,
    SORCERER_DRUID;
    
    @Override
    public String toString(){
        switch(this){
            case KNIGHT:
                return "Knight";
            case ELITE_KNIGHT:
                return "Elite Knight";
            case PALADIN:
                return "Paladin";
            case ROYAL_PALADIN:
                return "Royal Paladin";
            case SORCERER:
                return "Sorcerer";
            case MASTER_SORCERER:
                return "Master Sorcerer";
            case DRUID:
                return "Druid";
            case ELDER_DRUID:
                return "Elder Druid";
            case ALL:
                return "All vocations";
            case NO_VOCATION:
                return "No vocation";
            case KNIGHT_PALADIN:
                return "Knights and Paladins";
            case SORCERER_DRUID:
                return "Sorcerers and Druids";
        }
        return "None";
    }
}
