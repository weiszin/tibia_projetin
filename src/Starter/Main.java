/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Starter;

import BackEnd.Database;
import Enums.Equipments;
import java.io.IOException;

/**
 *
 * @author Ivan
 */
public class Main {   
    
    /**
     * @param args the command line arguments
     */
    
    static Database core;
    
    public static void main(String[] args) throws IOException {
        
        core = Database.getInstance();
        
        core.loadShield("ItemsInfo\\Shields", Equipments.SHIELD);
        core.loadShield("ItemsInfo\\Spellbooks", Equipments.SPELLBOOK);
        core.loadMeleeWeapons("ItemsInfo\\Swords", Equipments.WEAPON_SWORD);
        core.loadMeleeWeapons("ItemsInfo\\Axes", Equipments.WEAPON_AXE);
        core.loadMeleeWeapons("ItemsInfo\\Clubs", Equipments.WEAPON_CLUB);
        core.loadDistanceWeapons("ItemsInfo\\Crossbows", Equipments.WEAPON_CROSSBOW);
        core.loadDistanceWeapons("ItemsInfo\\Bows", Equipments.WEAPON_BOW);
        core.loadDistanceWeapons("ItemsInfo\\Throwables", Equipments.WEAPON_THROWABLE);
        core.loadMagicWeapons("ItemsInfo\\Wands", Equipments.WEAPON_WAND);
        core.loadMagicWeapons("ItemsInfo\\Rods", Equipments.WEAPON_ROD);
        core.loadDefenseSet("ItemsInfo\\Helmets", Equipments.HELMET);
        core.loadDefenseSet("ItemsInfo\\Armors", Equipments.ARMOR);
        core.loadDefenseSet("ItemsInfo\\Legs", Equipments.LEGS);
        core.loadDefenseSet("ItemsInfo\\Boots", Equipments.BOOTS);
        core.loadRingNecklace("ItemsInfo\\Amulets", Equipments.AMULET);
        core.loadRingNecklace("ItemsInfo\\Rings", Equipments.RING);
        core.loadAmmo("ItemsInfo\\Arrows", Equipments.AMMO_ARROW);
        core.loadAmmo("ItemsInfo\\Bolts", Equipments.AMMO_BOLT);
        
//        int idx = 28;
//        
//        System.out.println("Name: " + Database.getHelmets().get(idx).getName());
//        System.out.println("Level: " + Database.getHelmets().get(idx).getLevel());
//        System.out.println("Defense: " + Database.getHelmets().get(idx).getDefense());
//        System.out.println("Weight: " + Database.getHelmets().get(idx).getWeight());
//        System.out.println("Slots: " + Database.getHelmets().get(idx).getSlots());
//        System.out.println("SkillBonus: " + Database.getHelmets().get(idx).getSkillBonus());
//        System.out.println("Protection: " + Database.getHelmets().get(idx).getProtection());
//        System.out.println("Vocation: " + Database.getHelmets().get(idx).getVocation());
        
        new Telas.Principal().setVisible(true);
        
    }
    
}

