/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/*
    Dia 14/12/2018:
        - Criado a tela principal
        - Criado tela para seleção de itens do Character
        - Feito as primeiras lógicas de programação
        - Inserido algumas imagens dos itens que serão usados no programa
        
        O que ficou para fazer (TODO):
            - Inserir o faltante dos itens: Helmets, Backpack, Bows, Crossbows,
              Spear+Throwable, Rods, Wands, Legs, Ammos, Boots

*/

package Telas;

import java.awt.Image;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import Classes.Character;
import Enums.Vocation;
import java.awt.Color;
import java.awt.event.MouseEvent;
import java.awt.event.WindowEvent;
import java.text.DecimalFormat;
import javax.swing.JOptionPane;
import javax.swing.SwingUtilities;

/**
 *
 * @author Ivan
 */
public class Principal extends javax.swing.JFrame {
    
    int state_Monster = 0;
    int state_Weapon = 0;
    double speedModify = 1;
    double speedConstantModify = 0;
    Character character;
    /**
     * Creates new form Teste
     */
    public final void setNoWeapon() throws IOException{
        String url_NoWeaponRight = "Imagens\\No_Equipment\\NoWeaponRight.gif";
        BufferedImage image = ImageIO.read(new File(url_NoWeaponRight));
        Image noWeaponRight = image.getScaledInstance(45, 45, Image.SCALE_SMOOTH);
        lbl_Weapon.setIcon(new ImageIcon(noWeaponRight));
    }
    public final void setNoShield() throws IOException{
        String url_NoWeaponLeft = "Imagens\\No_Equipment\\NoWeaponLeft.gif";
        Image image = ImageIO.read(new File(url_NoWeaponLeft));
        Image noWeaponLeft = image.getScaledInstance(45, 45, Image.SCALE_SMOOTH);
        lbl_Shield.setIcon(new ImageIcon(noWeaponLeft));
    }
    public final void setNoAmulet() throws IOException{
        String url_NoAmulet = "Imagens\\No_Equipment\\NoAmulet.gif";
        Image image = ImageIO.read(new File(url_NoAmulet));
        Image noAmulet = image.getScaledInstance(45, 45, Image.SCALE_SMOOTH);
        lbl_Amulet.setIcon(new ImageIcon(noAmulet));
    }
    public final void setNoRing() throws IOException{
        String url_NoRing = "Imagens\\No_Equipment\\NoRing.gif";
        Image image = ImageIO.read(new File(url_NoRing));
        Image noRing = image.getScaledInstance(45, 45, Image.SCALE_SMOOTH);
        lbl_Ring.setIcon(new ImageIcon(noRing));
    }
    public final void setNoHelmet() throws IOException{
        String url_NoHelmet = "Imagens\\No_Equipment\\NoHelmet.gif";
        Image image = ImageIO.read(new File(url_NoHelmet));
        Image noHelmet = image.getScaledInstance(45, 45, Image.SCALE_SMOOTH);
        lbl_Helmet.setIcon(new ImageIcon(noHelmet));
    }
    public final void setNoArmor() throws IOException{
        String url_NoArmor = "Imagens\\No_Equipment\\NoArmor.gif";
        Image image = ImageIO.read(new File(url_NoArmor));
        Image noArmor = image.getScaledInstance(45, 45, Image.SCALE_SMOOTH);
        lbl_Armor.setIcon(new ImageIcon(noArmor));
    }
    public final void setNoLegs() throws IOException{
        String url_NoLegs = "Imagens\\No_Equipment\\NoLegs.gif";
        Image image = ImageIO.read(new File(url_NoLegs));
        Image noLegs = image.getScaledInstance(45, 45, Image.SCALE_SMOOTH);
        lbl_Legs.setIcon(new ImageIcon(noLegs));
    }
    public final void setNoBoots() throws IOException{
        String url_NoBoots = "Imagens\\No_Equipment\\NoBoots.gif";
        Image image = ImageIO.read(new File(url_NoBoots));
        Image noBoots = image.getScaledInstance(45, 45, Image.SCALE_SMOOTH);
        lbl_Boots.setIcon(new ImageIcon(noBoots));
    }
    public final void setNoAmmo() throws IOException{
        String url_NoAmmo = "Imagens\\No_Equipment\\NoAmmo.gif";
        Image image = ImageIO.read(new File(url_NoAmmo));
        Image noAmmo = image.getScaledInstance(45, 45, Image.SCALE_SMOOTH);
        lbl_Ammo.setIcon(new ImageIcon(noAmmo));
    }
    public final void setNoBackpack() throws IOException{
        String url_NoBackpack = "Imagens\\No_Equipment\\NoBag.gif";
        Image image = ImageIO.read(new File(url_NoBackpack));
        Image noBackpack = image.getScaledInstance(45, 45, Image.SCALE_SMOOTH);
        lbl_Backpack.setIcon(new ImageIcon(noBackpack));
    }
    public final void setCharacterBasics(){
        character.setVocation(Vocation.NO_VOCATION);
        character.setLevel(Integer.parseInt(txf_Level.getText()));
        character.setMagicLevel(Integer.parseInt(txf_MagicLevel.getText()));
        character.setSwordSkill(Integer.parseInt(txf_SwordSkill.getText()));
        character.setAxeSkill(Integer.parseInt(txf_AxeSkill.getText()));
        character.setClubSkill(Integer.parseInt(txf_ClubSkill.getText()));
        character.setDistanceSkill(Integer.parseInt(txf_DistanceSkill.getText()));
    }
    public Principal() throws IOException {
        initComponents();
        
        character = Classes.Character.getInstance();
        // Setando todos os espaços como nada equipado
        setCharacterBasics();
        updateCharacterSet();
//        setNoWeapon();
//        setNoShield();
//        setNoHelmet();
//        setNoArmor();
//        setNoLegs();
//        setNoBoots();
//        setNoAmulet();
//        setNoRing();
//        setNoAmmo();
//        setNoBackpack();
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jScrollBar1 = new javax.swing.JScrollBar();
        buttonGroup1 = new javax.swing.ButtonGroup();
        buttonGroup2 = new javax.swing.ButtonGroup();
        jTabbedPane1 = new javax.swing.JTabbedPane();
        jpn_Character = new javax.swing.JPanel();
        lbl_Legs = new javax.swing.JLabel();
        lbl_Helmet = new javax.swing.JLabel();
        lbl_Armor = new javax.swing.JLabel();
        lbl_Boots = new javax.swing.JLabel();
        lbl_Shield = new javax.swing.JLabel();
        lbl_Weapon = new javax.swing.JLabel();
        lbl_Ring = new javax.swing.JLabel();
        lbl_Amulet = new javax.swing.JLabel();
        lbl_Backpack = new javax.swing.JLabel();
        lbl_Ammo = new javax.swing.JLabel();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        txf_Level = new javax.swing.JFormattedTextField();
        txf_MagicLevel = new javax.swing.JFormattedTextField();
        txf_SwordSkill = new javax.swing.JFormattedTextField();
        txf_AxeSkill = new javax.swing.JFormattedTextField();
        txf_ClubSkill = new javax.swing.JFormattedTextField();
        txf_DistanceSkill = new javax.swing.JFormattedTextField();
        rad_Knight = new javax.swing.JRadioButton();
        rad_Paladin = new javax.swing.JRadioButton();
        rad_Sorcerer = new javax.swing.JRadioButton();
        rad_Druid = new javax.swing.JRadioButton();
        rad_NoVocation = new javax.swing.JRadioButton();
        jpn_Statistics = new javax.swing.JPanel();
        jLabel7 = new javax.swing.JLabel();
        txf_ExpNextLevel = new javax.swing.JTextField();
        jLabel8 = new javax.swing.JLabel();
        txf_ManaNextML = new javax.swing.JTextField();
        jLabel9 = new javax.swing.JLabel();
        txf_HitsNextMelee = new javax.swing.JTextField();
        cbx_SkillMelee = new javax.swing.JComboBox<>();
        jLabel10 = new javax.swing.JLabel();
        jLabel11 = new javax.swing.JLabel();
        jLabel12 = new javax.swing.JLabel();
        txf_QuantidadeHP = new javax.swing.JTextField();
        txf_QuantidadeMP = new javax.swing.JTextField();
        txf_Capacidade = new javax.swing.JTextField();
        jPanel1 = new javax.swing.JPanel();
        jLabel13 = new javax.swing.JLabel();
        txf_BasicSpeed = new javax.swing.JTextField();
        rad_NoModify = new javax.swing.JRadioButton();
        rad_Haste = new javax.swing.JRadioButton();
        rad_StrongHaste = new javax.swing.JRadioButton();
        rad_SwiftFoot = new javax.swing.JRadioButton();
        rad_Charge = new javax.swing.JRadioButton();
        jLabel14 = new javax.swing.JLabel();
        txf_TempoSQM = new javax.swing.JTextField();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setResizable(false);
        addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowActivated(java.awt.event.WindowEvent evt) {
                formWindowActivated(evt);
            }
        });

        lbl_Legs.setBackground(new java.awt.Color(0, 51, 51));
        lbl_Legs.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lbl_Legs.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        lbl_Legs.setOpaque(true);
        lbl_Legs.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                legsMouseClicked(evt);
            }
        });

        lbl_Helmet.setBackground(new java.awt.Color(0, 51, 51));
        lbl_Helmet.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lbl_Helmet.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        lbl_Helmet.setOpaque(true);
        lbl_Helmet.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                helmetMouseClicked(evt);
            }
        });

        lbl_Armor.setBackground(new java.awt.Color(0, 51, 51));
        lbl_Armor.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lbl_Armor.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        lbl_Armor.setOpaque(true);
        lbl_Armor.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                armorMouseClicked(evt);
            }
        });

        lbl_Boots.setBackground(new java.awt.Color(0, 51, 51));
        lbl_Boots.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lbl_Boots.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        lbl_Boots.setOpaque(true);
        lbl_Boots.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                bootsMouseClicked(evt);
            }
        });

        lbl_Shield.setBackground(new java.awt.Color(0, 51, 51));
        lbl_Shield.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lbl_Shield.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        lbl_Shield.setOpaque(true);
        lbl_Shield.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                shieldMouseClicked(evt);
            }
        });

        lbl_Weapon.setBackground(new java.awt.Color(0, 51, 51));
        lbl_Weapon.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lbl_Weapon.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        lbl_Weapon.setOpaque(true);
        lbl_Weapon.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                WeaponMouseClicked(evt);
            }
        });

        lbl_Ring.setBackground(new java.awt.Color(0, 51, 51));
        lbl_Ring.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lbl_Ring.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        lbl_Ring.setOpaque(true);
        lbl_Ring.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                ringMouseClicked(evt);
            }
        });

        lbl_Amulet.setBackground(new java.awt.Color(0, 51, 51));
        lbl_Amulet.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lbl_Amulet.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        lbl_Amulet.setOpaque(true);
        lbl_Amulet.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                amuletMouseClicked(evt);
            }
        });

        lbl_Backpack.setBackground(new java.awt.Color(0, 51, 51));
        lbl_Backpack.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lbl_Backpack.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        lbl_Backpack.setOpaque(true);
        lbl_Backpack.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                backpackMouseClicked(evt);
            }
        });

        lbl_Ammo.setBackground(new java.awt.Color(0, 51, 51));
        lbl_Ammo.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lbl_Ammo.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        lbl_Ammo.setOpaque(true);
        lbl_Ammo.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                ammoMouseClicked(evt);
            }
        });

        jLabel1.setText("Level:");

        jLabel2.setText("Magic Level:");

        jLabel3.setText("Sword Skill:");

        jLabel4.setText("Axe Skill:");

        jLabel5.setText("Club Skill:");

        jLabel6.setText("Distance Skill:");

        txf_Level.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.NumberFormatter(new java.text.DecimalFormat("#"))));
        txf_Level.setText("8");
        txf_Level.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusLost(java.awt.event.FocusEvent evt) {
                txf_LevelFocusLost(evt);
            }
        });

        txf_MagicLevel.setText("0");
        txf_MagicLevel.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusLost(java.awt.event.FocusEvent evt) {
                txf_MagicLevelFocusLost(evt);
            }
        });

        txf_SwordSkill.setText("10");
        txf_SwordSkill.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusLost(java.awt.event.FocusEvent evt) {
                txf_SwordSkillFocusLost(evt);
            }
        });

        txf_AxeSkill.setText("10");
        txf_AxeSkill.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusLost(java.awt.event.FocusEvent evt) {
                txf_AxeSkillFocusLost(evt);
            }
        });

        txf_ClubSkill.setText("10");
        txf_ClubSkill.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusLost(java.awt.event.FocusEvent evt) {
                txf_ClubSkillFocusLost(evt);
            }
        });

        txf_DistanceSkill.setText("10");
        txf_DistanceSkill.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusLost(java.awt.event.FocusEvent evt) {
                txf_DistanceSkillFocusLost(evt);
            }
        });

        buttonGroup1.add(rad_Knight);
        rad_Knight.setText("Knight");
        rad_Knight.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                rad_KnightActionPerformed(evt);
            }
        });

        buttonGroup1.add(rad_Paladin);
        rad_Paladin.setText("Paladin");
        rad_Paladin.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                rad_PaladinActionPerformed(evt);
            }
        });

        buttonGroup1.add(rad_Sorcerer);
        rad_Sorcerer.setText("Sorcerer");
        rad_Sorcerer.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                rad_SorcererActionPerformed(evt);
            }
        });

        buttonGroup1.add(rad_Druid);
        rad_Druid.setText("Druid");
        rad_Druid.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                rad_DruidActionPerformed(evt);
            }
        });

        buttonGroup1.add(rad_NoVocation);
        rad_NoVocation.setSelected(true);
        rad_NoVocation.setText("No vocation");
        rad_NoVocation.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                rad_NoVocationActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jpn_CharacterLayout = new javax.swing.GroupLayout(jpn_Character);
        jpn_Character.setLayout(jpn_CharacterLayout);
        jpn_CharacterLayout.setHorizontalGroup(
            jpn_CharacterLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jpn_CharacterLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jpn_CharacterLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(rad_Knight)
                    .addComponent(rad_Paladin)
                    .addComponent(rad_Sorcerer)
                    .addComponent(rad_Druid)
                    .addComponent(rad_NoVocation))
                .addGap(18, 18, 18)
                .addGroup(jpn_CharacterLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(lbl_Amulet, javax.swing.GroupLayout.PREFERRED_SIZE, 45, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lbl_Weapon, javax.swing.GroupLayout.PREFERRED_SIZE, 45, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lbl_Ring, javax.swing.GroupLayout.PREFERRED_SIZE, 45, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jpn_CharacterLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(lbl_Boots, javax.swing.GroupLayout.PREFERRED_SIZE, 45, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lbl_Helmet, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 45, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lbl_Legs, javax.swing.GroupLayout.PREFERRED_SIZE, 45, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lbl_Armor, javax.swing.GroupLayout.PREFERRED_SIZE, 45, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jpn_CharacterLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(lbl_Shield, javax.swing.GroupLayout.PREFERRED_SIZE, 45, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lbl_Backpack, javax.swing.GroupLayout.PREFERRED_SIZE, 45, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lbl_Ammo, javax.swing.GroupLayout.PREFERRED_SIZE, 45, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jpn_CharacterLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(jpn_CharacterLayout.createSequentialGroup()
                        .addComponent(jLabel6)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txf_DistanceSkill, javax.swing.GroupLayout.PREFERRED_SIZE, 61, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jpn_CharacterLayout.createSequentialGroup()
                        .addGroup(jpn_CharacterLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel2)
                            .addComponent(jLabel3)
                            .addComponent(jLabel1)
                            .addComponent(jLabel4)
                            .addComponent(jLabel5))
                        .addGap(10, 10, 10)
                        .addGroup(jpn_CharacterLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                            .addComponent(txf_AxeSkill, javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(txf_SwordSkill, javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(txf_MagicLevel, javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(txf_Level, javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(txf_ClubSkill, javax.swing.GroupLayout.PREFERRED_SIZE, 61, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addContainerGap(18, Short.MAX_VALUE))
        );
        jpn_CharacterLayout.setVerticalGroup(
            jpn_CharacterLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jpn_CharacterLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jpn_CharacterLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jpn_CharacterLayout.createSequentialGroup()
                        .addComponent(lbl_Helmet, javax.swing.GroupLayout.PREFERRED_SIZE, 45, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(lbl_Armor, javax.swing.GroupLayout.PREFERRED_SIZE, 45, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(lbl_Legs, javax.swing.GroupLayout.PREFERRED_SIZE, 45, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(lbl_Boots, javax.swing.GroupLayout.PREFERRED_SIZE, 45, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jpn_CharacterLayout.createSequentialGroup()
                        .addGroup(jpn_CharacterLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel1)
                            .addComponent(txf_Level, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGroup(jpn_CharacterLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jpn_CharacterLayout.createSequentialGroup()
                                .addGap(18, 18, 18)
                                .addGroup(jpn_CharacterLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                    .addComponent(jLabel2)
                                    .addComponent(txf_MagicLevel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                            .addGroup(jpn_CharacterLayout.createSequentialGroup()
                                .addGap(7, 7, 7)
                                .addComponent(lbl_Backpack, javax.swing.GroupLayout.PREFERRED_SIZE, 45, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jpn_CharacterLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addGroup(jpn_CharacterLayout.createSequentialGroup()
                                .addGroup(jpn_CharacterLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                    .addComponent(jLabel3)
                                    .addComponent(txf_SwordSkill, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGap(18, 18, 18)
                                .addGroup(jpn_CharacterLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                    .addComponent(jLabel4)
                                    .addComponent(txf_AxeSkill, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGap(18, 18, 18)
                                .addGroup(jpn_CharacterLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                    .addComponent(jLabel5)
                                    .addComponent(txf_ClubSkill, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                            .addGroup(jpn_CharacterLayout.createSequentialGroup()
                                .addComponent(lbl_Shield, javax.swing.GroupLayout.PREFERRED_SIZE, 45, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(lbl_Ammo, javax.swing.GroupLayout.PREFERRED_SIZE, 45, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addGap(18, 18, 18)
                        .addGroup(jpn_CharacterLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel6)
                            .addComponent(txf_DistanceSkill, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addGroup(jpn_CharacterLayout.createSequentialGroup()
                        .addGap(15, 15, 15)
                        .addGroup(jpn_CharacterLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(lbl_Amulet, javax.swing.GroupLayout.PREFERRED_SIZE, 45, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGroup(jpn_CharacterLayout.createSequentialGroup()
                                .addComponent(rad_NoVocation)
                                .addGap(14, 14, 14)
                                .addComponent(rad_Knight)))
                        .addGroup(jpn_CharacterLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jpn_CharacterLayout.createSequentialGroup()
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(lbl_Weapon, javax.swing.GroupLayout.PREFERRED_SIZE, 44, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(jpn_CharacterLayout.createSequentialGroup()
                                .addGap(15, 15, 15)
                                .addComponent(rad_Paladin)))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jpn_CharacterLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jpn_CharacterLayout.createSequentialGroup()
                                .addComponent(rad_Sorcerer)
                                .addGap(18, 18, 18)
                                .addComponent(rad_Druid))
                            .addComponent(lbl_Ring, javax.swing.GroupLayout.PREFERRED_SIZE, 45, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jTabbedPane1.addTab("Character", jpn_Character);

        jLabel7.setText("Experiência para avançar de nível:");

        txf_ExpNextLevel.setEditable(false);
        txf_ExpNextLevel.setHorizontalAlignment(javax.swing.JTextField.LEFT);

        jLabel8.setText("Quantidade de mana para avançar ML:");

        txf_ManaNextML.setEditable(false);

        jLabel9.setText("Quantidade de hits para avançar Melee:");

        txf_HitsNextMelee.setEditable(false);

        cbx_SkillMelee.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Sword", "Axe", "Club" }));
        cbx_SkillMelee.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                cbx_SkillMeleeItemStateChanged(evt);
            }
        });

        jLabel10.setText("Quantidade de HP:");

        jLabel11.setText("Quantidade de MP:");

        jLabel12.setText("Capacidade:");

        txf_QuantidadeHP.setEditable(false);

        txf_QuantidadeMP.setEditable(false);

        txf_Capacidade.setEditable(false);

        javax.swing.GroupLayout jpn_StatisticsLayout = new javax.swing.GroupLayout(jpn_Statistics);
        jpn_Statistics.setLayout(jpn_StatisticsLayout);
        jpn_StatisticsLayout.setHorizontalGroup(
            jpn_StatisticsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jpn_StatisticsLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jpn_StatisticsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jpn_StatisticsLayout.createSequentialGroup()
                        .addComponent(jLabel7)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txf_ExpNextLevel))
                    .addGroup(jpn_StatisticsLayout.createSequentialGroup()
                        .addComponent(jLabel8)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txf_ManaNextML))
                    .addGroup(jpn_StatisticsLayout.createSequentialGroup()
                        .addGroup(jpn_StatisticsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                            .addGroup(jpn_StatisticsLayout.createSequentialGroup()
                                .addComponent(jLabel11)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(txf_QuantidadeMP))
                            .addGroup(jpn_StatisticsLayout.createSequentialGroup()
                                .addComponent(jLabel10)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(txf_QuantidadeHP))
                            .addComponent(jLabel9))
                        .addGroup(jpn_StatisticsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jpn_StatisticsLayout.createSequentialGroup()
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(txf_HitsNextMelee, javax.swing.GroupLayout.PREFERRED_SIZE, 125, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(cbx_SkillMelee, 0, 62, Short.MAX_VALUE))
                            .addGroup(jpn_StatisticsLayout.createSequentialGroup()
                                .addGap(20, 20, 20)
                                .addComponent(jLabel12)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(txf_Capacidade)))))
                .addContainerGap())
        );
        jpn_StatisticsLayout.setVerticalGroup(
            jpn_StatisticsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jpn_StatisticsLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jpn_StatisticsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel7)
                    .addComponent(txf_ExpNextLevel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jpn_StatisticsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel8)
                    .addComponent(txf_ManaNextML, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jpn_StatisticsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel9)
                    .addComponent(txf_HitsNextMelee, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(cbx_SkillMelee, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(46, 46, 46)
                .addGroup(jpn_StatisticsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(jpn_StatisticsLayout.createSequentialGroup()
                        .addGroup(jpn_StatisticsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addGroup(jpn_StatisticsLayout.createSequentialGroup()
                                .addGroup(jpn_StatisticsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                    .addComponent(jLabel10)
                                    .addComponent(txf_QuantidadeHP, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGap(18, 18, 18))
                            .addGroup(jpn_StatisticsLayout.createSequentialGroup()
                                .addGroup(jpn_StatisticsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                    .addComponent(jLabel12)
                                    .addComponent(txf_Capacidade, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGap(7, 7, 7)))
                        .addComponent(jLabel11))
                    .addComponent(txf_QuantidadeMP, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(53, Short.MAX_VALUE))
        );

        jTabbedPane1.addTab("Stats - Skill/Level", jpn_Statistics);

        jLabel13.setText("Velocidade base:");

        txf_BasicSpeed.setEditable(false);

        buttonGroup2.add(rad_NoModify);
        rad_NoModify.setSelected(true);
        rad_NoModify.setText("Sem modificação");
        rad_NoModify.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                rad_NoModifyActionPerformed(evt);
            }
        });

        buttonGroup2.add(rad_Haste);
        rad_Haste.setText("Haste");
        rad_Haste.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                rad_HasteActionPerformed(evt);
            }
        });

        buttonGroup2.add(rad_StrongHaste);
        rad_StrongHaste.setText("Strong Haste");
        rad_StrongHaste.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                rad_StrongHasteActionPerformed(evt);
            }
        });

        buttonGroup2.add(rad_SwiftFoot);
        rad_SwiftFoot.setText("Swift Foot");
        rad_SwiftFoot.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                rad_SwiftFootActionPerformed(evt);
            }
        });

        buttonGroup2.add(rad_Charge);
        rad_Charge.setText("Charge");
        rad_Charge.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                rad_ChargeActionPerformed(evt);
            }
        });

        jLabel14.setText("Tempo 1 sqm:");

        txf_TempoSQM.setEditable(false);

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(jLabel13)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txf_BasicSpeed, javax.swing.GroupLayout.PREFERRED_SIZE, 126, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(jLabel14)
                        .addGap(18, 18, 18)
                        .addComponent(txf_TempoSQM)))
                .addGap(18, 18, 18)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(rad_Charge)
                    .addComponent(rad_SwiftFoot)
                    .addComponent(rad_StrongHaste)
                    .addComponent(rad_Haste)
                    .addComponent(rad_NoModify))
                .addContainerGap(68, Short.MAX_VALUE))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addGap(19, 19, 19)
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                    .addComponent(jLabel13)
                                    .addComponent(txf_BasicSpeed, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addContainerGap()
                                .addComponent(rad_NoModify)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(rad_Haste)))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(rad_StrongHaste))
                    .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jLabel14)
                        .addComponent(txf_TempoSQM, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(rad_SwiftFoot)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(rad_Charge)
                .addContainerGap(96, Short.MAX_VALUE))
        );

        jTabbedPane1.addTab("Stats - Speed", jPanel1);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jTabbedPane1)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jTabbedPane1)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void ammoMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_ammoMouseClicked
        // TODO add your handling code here:
        try {
            if(evt.getButton() == MouseEvent.BUTTON1){
                new Selecao_Ammo(this).setVisible(true);
            } else if(evt.getButton() == MouseEvent.BUTTON3){
                setNoAmmo();
                character.setAmmo(null);
                updateStatistic();
            }
        } catch (IOException ex) {
            Logger.getLogger(Principal.class.getName()).log(Level.SEVERE, null, ex);
        }
    }//GEN-LAST:event_ammoMouseClicked

    private void backpackMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_backpackMouseClicked
        // TODO add your handling code here:
        try {
            // TODO add your handling code here:
            setBackpackImage();
            new Selecao_Itens_Character(this, Enums.Equipments.BACKPACK).setVisible(true);

        } catch (IOException ex) {
            Logger.getLogger(Principal.class.getName()).log(Level.SEVERE, null, ex);
        }
    }//GEN-LAST:event_backpackMouseClicked

    private void amuletMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_amuletMouseClicked
        // TODO add your handling code here:
        try {
            // TODO add your handling code here:
            if(evt.getButton() == MouseEvent.BUTTON1){
                new Selecao_Itens_Character(this, Enums.Equipments.AMULET).setVisible(true);
            } else if(evt.getButton() == MouseEvent.BUTTON3){
                setNoAmulet();
                character.setAmulet(null);
                updateStatistic();
            }
        } catch (IOException ex) {
            Logger.getLogger(Principal.class.getName()).log(Level.SEVERE, null, ex);
        }
    }//GEN-LAST:event_amuletMouseClicked

    private void ringMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_ringMouseClicked
        // TODO add your handling code here:
        try {
            // TODO add your handling code here:
            if(evt.getButton() == MouseEvent.BUTTON1){
                new Selecao_Itens_Character(this, Enums.Equipments.RING).setVisible(true);
            } else if(evt.getButton() == MouseEvent.BUTTON3){
                setNoRing();
                character.setRing(null);
                updateStatistic();
            }
        } catch (IOException ex) {
            Logger.getLogger(Principal.class.getName()).log(Level.SEVERE, null, ex);
        }
    }//GEN-LAST:event_ringMouseClicked

    /**
     * Quando clicar no Weapon, abre uma janela para escolher qual item deseja
     * @param evt 
     */
    private void WeaponMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_WeaponMouseClicked
        // TODO add your handling code here:
        try {
            if(evt.getButton() == MouseEvent.BUTTON1){
                new Selecao_Weapon(this).setVisible(true);
            } else if(evt.getButton() == MouseEvent.BUTTON3){
                setNoWeapon();
                character.setMeleeWeapon(null);
                character.setDistanceWeapon(null);
                character.setMagicWeapon(null);
                updateStatistic();
            }
        } catch (IOException ex) {
            Logger.getLogger(Principal.class.getName()).log(Level.SEVERE, null, ex);
        }
    }//GEN-LAST:event_WeaponMouseClicked

    private void shieldMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_shieldMouseClicked
        try {
            if(evt.getButton() == MouseEvent.BUTTON1){
                new Selecao_Shield(this).setVisible(true);
            } else if(evt.getButton() == MouseEvent.BUTTON3){
                setNoShield();
                character.setShield(null);
                updateStatistic();
            }
        } catch (IOException ex) {
            Logger.getLogger(Principal.class.getName()).log(Level.SEVERE, null, ex);
        }
    }//GEN-LAST:event_shieldMouseClicked

    private void bootsMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_bootsMouseClicked
        // TODO add your handling code here:
        try {
            if(evt.getButton() == MouseEvent.BUTTON1){
                new Selecao_Itens_Character(this, Enums.Equipments.BOOTS).setVisible(true);
            } else if(evt.getButton() == MouseEvent.BUTTON3){
                setNoBoots();
                character.setBoots(null);
                updateStatistic();
            }
        } catch (IOException ex) {
            Logger.getLogger(Principal.class.getName()).log(Level.SEVERE, null, ex);
        }
    }//GEN-LAST:event_bootsMouseClicked

    private void armorMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_armorMouseClicked
        // TODO add your handling code here:
        try {
            // TODO add your handling code here:
            if(evt.getButton() == MouseEvent.BUTTON1){
                new Selecao_Itens_Character(this, Enums.Equipments.ARMOR).setVisible(true);
            } else if(evt.getButton() == MouseEvent.BUTTON3){
                setNoArmor();
                character.setArmor(null);
                updateStatistic();
            }
        } catch (IOException ex) {
            Logger.getLogger(Principal.class.getName()).log(Level.SEVERE, null, ex);
        }
    }//GEN-LAST:event_armorMouseClicked

    private void helmetMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_helmetMouseClicked
        // TODO add your handling code here:
        try {
            // TODO add your handling code here:
            if(evt.getButton() == MouseEvent.BUTTON1){
                new Selecao_Itens_Character(this, Enums.Equipments.HELMET).setVisible(true);
            } else if(evt.getButton() == MouseEvent.BUTTON3){
                setNoHelmet();
                character.setHelmet(null);
                updateStatistic();
            }
        } catch (IOException ex) {
            Logger.getLogger(Principal.class.getName()).log(Level.SEVERE, null, ex);
        }
    }//GEN-LAST:event_helmetMouseClicked

    private void legsMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_legsMouseClicked
        // TODO add your handling code here:
        try {
            // TODO add your handling code here:
            if(evt.getButton() == MouseEvent.BUTTON1){
                new Selecao_Itens_Character(this, Enums.Equipments.LEGS).setVisible(true);
            } else if(evt.getButton() == MouseEvent.BUTTON3){
                setNoLegs();
                character.setLegs(null);
                updateStatistic();
            }
        } catch (IOException ex) {
            Logger.getLogger(Principal.class.getName()).log(Level.SEVERE, null, ex);
        }
    }//GEN-LAST:event_legsMouseClicked

    private void setWeaponImage() throws IOException{
        if(character.getMeleeWeapon() != null){
            lbl_Weapon.setIcon(character.getMeleeWeapon().getImage());
        } else if(character.getDistanceWeapon() != null){
            lbl_Weapon.setIcon(character.getDistanceWeapon().getImage());
        } else if(character.getMagicWeapon() != null){
            lbl_Weapon.setIcon(character.getMagicWeapon().getImage());
        } else {
            setNoWeapon();
        }
    }
    private void setShieldImage() throws IOException{
        if(character.getShield() != null) lbl_Shield.setIcon(character.getShield().getImage());
        else setNoShield();
    }
    private void setHelmetImage() throws IOException{
        if(character.getHelmet() != null) lbl_Helmet.setIcon(character.getHelmet().getImage());
        else setNoHelmet();
    }
    private void setArmorImage() throws IOException{
        if(character.getArmor() != null) lbl_Armor.setIcon(character.getArmor().getImage());
        else setNoArmor();
    }
    private void setLegsImage() throws IOException{
        if(character.getLegs() != null) lbl_Legs.setIcon(character.getLegs().getImage());
        else setNoLegs();
    }
    private void setBootsImage() throws IOException{
        if(character.getBoots() != null) lbl_Boots.setIcon(character.getBoots().getImage());
        else setNoBoots();
    }
    private void setAmuletImage() throws IOException{
        if(character.getAmulet() != null) lbl_Amulet.setIcon(character.getAmulet().getImage());
        else setNoAmulet();
    }
    private void setRingImage() throws IOException{
        if(character.getRing() != null) lbl_Ring.setIcon(character.getRing().getImage());
        else setNoRing();
    }
    private void setAmmoImage() throws IOException{
        if(character.getAmmo() != null) lbl_Ammo.setIcon(character.getAmmo().getImage());
        else setNoAmmo();
    }
    private void setBackpackImage() throws IOException{
        setNoBackpack();
    }
    private void updateCharacterSet(){
        try {
            setWeaponImage();
            setShieldImage();
            setHelmetImage();
            setArmorImage();
            setLegsImage();
            setBootsImage();
            setAmuletImage();
            setRingImage();
            setAmmoImage();
            setBackpackImage();
            updateStatistic();
        } catch (IOException ex){
            JOptionPane.showMessageDialog(null, ex.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
        }
    }
    
    private void txf_LevelFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_txf_LevelFocusLost
        // TODO add your handling code here:
        int level = 0;
        try {
            level = new Integer(txf_Level.getText());
            character.setLevel(level);
            updateSpeedModifies();
            updateStatistic();
        } catch(Exception ex){
            
        }
    }//GEN-LAST:event_txf_LevelFocusLost

    private void txf_MagicLevelFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_txf_MagicLevelFocusLost
        // TODO add your handling code here:
        int magicLevel = 0;
        try {
            magicLevel = new Integer(txf_MagicLevel.getText());
            character.setMagicLevel(magicLevel);
            updateStatistic();
        } catch(Exception ex){
            
        }
    }//GEN-LAST:event_txf_MagicLevelFocusLost

    private void txf_SwordSkillFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_txf_SwordSkillFocusLost
        // TODO add your handling code here:
        int swordSkill = 0;
        try {
            swordSkill = new Integer(txf_SwordSkill.getText());
            character.setSwordSkill(swordSkill);
            updateStatistic();
        } catch(Exception ex){
            
        }
    }//GEN-LAST:event_txf_SwordSkillFocusLost

    private void txf_AxeSkillFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_txf_AxeSkillFocusLost
        // TODO add your handling code here:
        int axeSkill = 0;
        try {
            axeSkill = new Integer(txf_AxeSkill.getText());
            character.setAxeSkill(axeSkill);
            updateStatistic();
        } catch(Exception ex){
            
        }
    }//GEN-LAST:event_txf_AxeSkillFocusLost

    private void txf_ClubSkillFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_txf_ClubSkillFocusLost
        // TODO add your handling code here:
        int clubSkill = 0;
        try {
            clubSkill = new Integer(txf_ClubSkill.getText());
            character.setClubSkill(clubSkill);
            updateStatistic();
        } catch(Exception ex){
            
        }
    }//GEN-LAST:event_txf_ClubSkillFocusLost

    private void txf_DistanceSkillFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_txf_DistanceSkillFocusLost
        // TODO add your handling code here:
        int distanceSkill = 0;
        try {
            distanceSkill = new Integer(txf_DistanceSkill.getText());
            character.setDistanceSkill(distanceSkill);
            updateStatistic();
        } catch(Exception ex){
            
        }
    }//GEN-LAST:event_txf_DistanceSkillFocusLost

    private void rad_KnightActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_rad_KnightActionPerformed
        // TODO add your handling code here:
        character.setVocation(Vocation.KNIGHT);
        updateStatistic();
    }//GEN-LAST:event_rad_KnightActionPerformed

    private void rad_PaladinActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_rad_PaladinActionPerformed
        // TODO add your handling code here:
        character.setVocation(Vocation.PALADIN);
        updateStatistic();
    }//GEN-LAST:event_rad_PaladinActionPerformed

    private void rad_NoVocationActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_rad_NoVocationActionPerformed
        // TODO add your handling code here:
        character.setVocation(Vocation.NO_VOCATION);
        updateStatistic();
    }//GEN-LAST:event_rad_NoVocationActionPerformed

    private void rad_SorcererActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_rad_SorcererActionPerformed
        // TODO add your handling code here:
        character.setVocation(Vocation.SORCERER);
        updateStatistic();
    }//GEN-LAST:event_rad_SorcererActionPerformed

    private void rad_DruidActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_rad_DruidActionPerformed
        // TODO add your handling code here:
        character.setVocation(Vocation.DRUID);
        updateStatistic();
    }//GEN-LAST:event_rad_DruidActionPerformed

    private void cbx_SkillMeleeItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_cbx_SkillMeleeItemStateChanged
        // TODO add your handling code here:
        updateHitsNextMelee();
    }//GEN-LAST:event_cbx_SkillMeleeItemStateChanged

    private void rad_StrongHasteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_rad_StrongHasteActionPerformed
        // TODO add your handling code here:
        speedModify = 1.7;
        speedConstantModify = -56;
        updateBasicSpeed();
    }//GEN-LAST:event_rad_StrongHasteActionPerformed

    private void rad_NoModifyActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_rad_NoModifyActionPerformed
        // TODO add your handling code here:
        speedModify = 1;
        speedConstantModify = 0;
        updateBasicSpeed();
    }//GEN-LAST:event_rad_NoModifyActionPerformed
    
    private void rad_HasteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_rad_HasteActionPerformed
        // TODO add your handling code here:
        speedModify = 1.3;
        speedConstantModify = -24;
        updateBasicSpeed();
    }//GEN-LAST:event_rad_HasteActionPerformed

    private void rad_SwiftFootActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_rad_SwiftFootActionPerformed
        // TODO add your handling code here:
        speedModify = 1;
        speedConstantModify = Math.round((character.getLevel() * 1.6 + 110)/2) * 2;
        updateBasicSpeed();
    }//GEN-LAST:event_rad_SwiftFootActionPerformed

    private void rad_ChargeActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_rad_ChargeActionPerformed
        // TODO add your handling code here:
        speedModify = 1;
        speedConstantModify = Math.round((character.getLevel() * 1.8 + 123.3) / 2) * 2;
        updateBasicSpeed();
    }//GEN-LAST:event_rad_ChargeActionPerformed
    
    private void updateSpeedModifies(){
        if(rad_Charge.isSelected()){
            speedConstantModify = Math.round((character.getLevel() * 1.8 + 123.3) / 2) * 2;
        } else if(rad_SwiftFoot.isSelected()){
            speedConstantModify = Math.round((character.getLevel() * 1.6 + 110)/2) * 2;
        }
    }
    
    private void formWindowActivated(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_formWindowActivated
        // TODO add your handling code here:
        updateCharacterSet();
        updateStatistic();
    }//GEN-LAST:event_formWindowActivated
    
    private void updateExpNextLevel(){
        int level = character.getLevel();
        Long nextLevel = new Long(50 * level * level - 150 * level + 200);
        txf_ExpNextLevel.setText(nextLevel.toString());
    }
    
    private void updateManaNextML(){
        int ml = character.getMagicLevel();
        Vocation voc = character.getVocation();
        double b = 0;
        switch(voc){
            case KNIGHT:
                b = 3;
                break;
            case PALADIN:
                b = 1.4;
                break;
            case SORCERER:
            case DRUID:
                b = 1.1;
                break;
            case NO_VOCATION:
                b = -1;
                break;
        }
        if(b == -1){
            txf_ManaNextML.setText("NaN");
        } else {
            Long manaNextML = (long) (1600 * Math.pow(b, ml));
            txf_ManaNextML.setText(manaNextML.toString());
        }
    }
    
    private void updateHitsNextMelee(){
        int meleeSkill = 0;
        int idx = cbx_SkillMelee.getSelectedIndex();
        if(idx == 0){
            meleeSkill = character.getSwordSkill();
        } else if(idx == 1){
            meleeSkill = character.getAxeSkill();
        } else if(idx == 2){
            meleeSkill = character.getClubSkill();
        }
        Vocation voc = character.getVocation();
        double b = 0;
        switch(voc){
            case KNIGHT:
                b = 1.1;
                break;
            case PALADIN:
                b = 1.2;
                break;
            case SORCERER:
            case NO_VOCATION:
                b = 2;
                break;
            case DRUID:
                b = 1.8;
                break;
        }
        Long hits = (long) (50 * Math.pow(b, meleeSkill - 10));
        txf_HitsNextMelee.setText(hits.toString());
    }
    
    private void updateQuantidadeHP(){
        int level = character.getLevel();
        Vocation voc = character.getVocation();
        Long totalLife = 0L;
        switch(voc){
            case KNIGHT:
                totalLife = (long) 5 * (3 * level + 13);
                break;
            case PALADIN:
                totalLife = (long) 5 * (2 * level + 21);
                break;
            case SORCERER:
            case DRUID:
            case NO_VOCATION:
                totalLife = (long) 5 * (level + 29);
                break;
        }
        txf_QuantidadeHP.setText(totalLife.toString());
    }
    private void updateQuantidadeMP(){
        int level = character.getLevel();
        Vocation voc = character.getVocation();
        Long totalMana = 0L;
        switch(voc){
            case KNIGHT:
                totalMana = (long) 5 * (level);
                break;
            case PALADIN:
                totalMana = (long) (level - 8) * 15 + 90;
                break;
            case SORCERER:
            case DRUID:
                totalMana = (long) (level - 8) * 30 + 90;
                break;
            case NO_VOCATION:
                totalMana = (long) 5 * (level);
                break;
        }
        txf_QuantidadeMP.setText(totalMana.toString());
    }
    private void updateCapacidade(){
        int level = character.getLevel();
        Vocation voc = character.getVocation();
        Long totalCap = 0L;
        switch(voc){
            case KNIGHT:
                totalCap = (long) 5 * (5 * level + 54);
                break;
            case PALADIN:
                totalCap = (long) 10 * (2 * level + 31);
                break;
            case SORCERER:
            case DRUID:
            case NO_VOCATION:
                totalCap = (long) 10 * (level + 39);
                break;
        }
        txf_Capacidade.setText(totalCap.toString());
    }
    
    private void updateBasicSpeed(){
        int level = character.getLevel();
        int basicSpeed = 220 + (2 * (level - 1));
        int finalSpeed = (int) Math.round((basicSpeed * speedModify + speedConstantModify) / 2);
        finalSpeed += character.getSpeedBonus();
        txf_BasicSpeed.setText(Integer.toString(finalSpeed));
        
        double tempoSQM = 100.0/finalSpeed;
        tempoSQM *= 100;
        tempoSQM = Math.round(tempoSQM);
        tempoSQM /= 100.0;
        txf_TempoSQM.setText(Double.toString(tempoSQM) + "s");
    }
            
    private void updateStatistic(){
        updateExpNextLevel();
        updateManaNextML();
        updateHitsNextMelee();
        updateQuantidadeHP();
        updateQuantidadeMP();
        updateCapacidade();
        updateBasicSpeed();
    }
    
    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Principal.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Principal.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Principal.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Principal.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                try {
                    new Principal().setVisible(true);
                } catch (IOException ex) {
                    Logger.getLogger(Principal.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.ButtonGroup buttonGroup1;
    private javax.swing.ButtonGroup buttonGroup2;
    private javax.swing.JComboBox<String> cbx_SkillMelee;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel14;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JScrollBar jScrollBar1;
    private javax.swing.JTabbedPane jTabbedPane1;
    private javax.swing.JPanel jpn_Character;
    private javax.swing.JPanel jpn_Statistics;
    private javax.swing.JLabel lbl_Ammo;
    private javax.swing.JLabel lbl_Amulet;
    private javax.swing.JLabel lbl_Armor;
    private javax.swing.JLabel lbl_Backpack;
    private javax.swing.JLabel lbl_Boots;
    private javax.swing.JLabel lbl_Helmet;
    private javax.swing.JLabel lbl_Legs;
    private javax.swing.JLabel lbl_Ring;
    private javax.swing.JLabel lbl_Shield;
    private javax.swing.JLabel lbl_Weapon;
    private javax.swing.JRadioButton rad_Charge;
    private javax.swing.JRadioButton rad_Druid;
    private javax.swing.JRadioButton rad_Haste;
    private javax.swing.JRadioButton rad_Knight;
    private javax.swing.JRadioButton rad_NoModify;
    private javax.swing.JRadioButton rad_NoVocation;
    private javax.swing.JRadioButton rad_Paladin;
    private javax.swing.JRadioButton rad_Sorcerer;
    private javax.swing.JRadioButton rad_StrongHaste;
    private javax.swing.JRadioButton rad_SwiftFoot;
    private javax.swing.JFormattedTextField txf_AxeSkill;
    private javax.swing.JTextField txf_BasicSpeed;
    private javax.swing.JTextField txf_Capacidade;
    private javax.swing.JFormattedTextField txf_ClubSkill;
    private javax.swing.JFormattedTextField txf_DistanceSkill;
    private javax.swing.JTextField txf_ExpNextLevel;
    private javax.swing.JTextField txf_HitsNextMelee;
    private javax.swing.JFormattedTextField txf_Level;
    private javax.swing.JFormattedTextField txf_MagicLevel;
    private javax.swing.JTextField txf_ManaNextML;
    private javax.swing.JTextField txf_QuantidadeHP;
    private javax.swing.JTextField txf_QuantidadeMP;
    private javax.swing.JFormattedTextField txf_SwordSkill;
    private javax.swing.JTextField txf_TempoSQM;
    // End of variables declaration//GEN-END:variables
}
