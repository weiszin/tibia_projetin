/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Telas;

import BackEnd.Database;
import Classes.Ammo;
import Classes.DefenseSet;
import Classes.DistanceWeapon;
import Classes.MagicWeapon;
import Classes.MeleeWeapon;
import Classes.RingAmulet;
import Classes.Shield;
import Classes.Character;
import java.awt.Color;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.WindowEvent;
import java.io.IOException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.border.LineBorder;

/**
 *
 * @author Ivan
 */
public class Selecao_Itens_Character extends javax.swing.JFrame {

    /**
     * Creates new form Selecao_Itens_Character
     */
    static JFrame parent_Window = null;
    JFrame me = this;
//    int tam_x_grid, tam_y_grid, qtd_itens;
    
    Character character;
    
    public Selecao_Itens_Character() throws IOException {
        this.character = Character.getInstance();
        initComponents();        
    }
    
    public Selecao_Itens_Character(Enums.Equipments item) throws IOException {
        this.character = Character.getInstance();
        initComponents();
        addImages(item);
        
        jpn_Imagens.addMouseListener(new MouseAdapter() {
            
            @Override
            public void mousePressed(MouseEvent evt){
                int verticalScrollValue = jpn_Imagens.getVerticalScrollBar().getValue();
//                System.out.println("X: " + evt.getPoint().x + " Y: " + (evt.getPoint().y + verticalScrollValue));
//                int x = evt.getPoint().x / tam_x_grid;
//                int y = (evt.getPoint().y + verticalScrollValue) / tam_y_grid;
                JLabel c = (JLabel) jpn_Imagens.getComponentAt(evt.getPoint());
                System.out.println(c.getText());
//                System.out.println("Xc: " + x + " Yc: " + y);
            }
        });
    }
    
    public Selecao_Itens_Character(JFrame janela, Enums.Equipments item) throws IOException {
        this.character = Character.getInstance();
        initComponents();
        parent_Window = janela;
        parent_Window.setEnabled(false);
        
        addImages(item);
        
//        tam_x_grid = Math.max(tam_x_grid, jpn_Imagens.getWidth() / 4);
        
//        jpn_Imagens.addMouseListener(new MouseAdapter() {
//            
//            @Override
//            public void mousePressed(MouseEvent evt){
//                int verticalScrollValue = jpn_Imagens.getVerticalScrollBar().getValue();
//                int horizontalScrollValue = jpn_Imagens.getHorizontalScrollBar().getValue();
////                System.out.println("X: " + evt.getPoint().x + " Y: " + (evt.getPoint().y + verticalScrollValue));
//                int x = (evt.getPoint().x + horizontalScrollValue) / tam_x_grid;
//                int y = (evt.getPoint().y + verticalScrollValue) / tam_y_grid;
////                JLabel c = (JLabel) jpn_Imagens.getComponentAt(evt.getPoint());
//                System.out.println(x + " " + y);
//            }
//        });
    }

    /**
     * Adiciona imagens ao ScrollPanel da aba Character
     * @param index
     * @throws IOException 
     */
    private void addImages(Enums.Equipments typeEquipment) throws IOException{
        JPanel jpn_Tmp = new JPanel(new GridLayout(0, 4));
        ArrayList<?> lista = new ArrayList<>();
//        String path = "";
//        File folder;
//        File[] listOfFiles = null;
        switch (typeEquipment){
            case WEAPON_SWORD:
            case WEAPON_AXE:
            case WEAPON_CLUB:
                switch(typeEquipment){
                    case WEAPON_SWORD:
                        lista = Database.getSwords();
                        break;
                    case WEAPON_AXE:
                        lista = Database.getAxes();
                        break;
                    case WEAPON_CLUB:
                        lista = Database.getClubs();
                        break;
                }
                for(int i = 0; i < lista.size(); i++){
                    JLabel picLabel;
                    MeleeWeapon mw = (MeleeWeapon) lista.get(i);
                    picLabel = new JLabel(((MeleeWeapon)lista.get(i)).getImage());
                    picLabel.setBorder(new LineBorder(Color.black));
                    picLabel.setText(((MeleeWeapon)lista.get(i)).getName());
                    picLabel.setHorizontalTextPosition(JLabel.CENTER);
                    picLabel.setVerticalTextPosition(JLabel.BOTTOM);
                    picLabel.setFont(new Font("Serif", Font.PLAIN, 10));
                    picLabel.addMouseListener(new MouseAdapter() {
                        @Override
                        public void mousePressed(MouseEvent evt){
                            character.setMeleeWeapon(mw);
                            //JOptionPane.showMessageDialog(null, mw.getName() + " foi selecionado!", "Sucesso!", JOptionPane.INFORMATION_MESSAGE);
                            me.dispatchEvent(new WindowEvent(me, WindowEvent.WINDOW_CLOSING));
                        }
                    });
//                    AffineTransform affinetransform = new AffineTransform();     
//                    FontRenderContext frc = new FontRenderContext(affinetransform,true,true);     
//                    tam_x_grid = (int) Math.max(tam_x_grid, picLabel.getFont().getStringBounds(picLabel.getText(), frc).getWidth());
//                    tam_y_grid = (int) Math.max(tam_y_grid, picLabel.getIcon().getIconHeight() + picLabel.getFont().getStringBounds(picLabel.getText(), frc).getHeight());
                    jpn_Tmp.add(picLabel);
                }
                break;
            case WEAPON_BOW:
            case WEAPON_CROSSBOW:
            case WEAPON_THROWABLE:
                switch(typeEquipment){
                    case WEAPON_BOW:
                        lista = Database.getBows();
                        break;
                    case WEAPON_CROSSBOW:
                        lista = Database.getCrossbows();
                        break;
                    case WEAPON_THROWABLE:
                        lista = Database.getThrowables();
                        break;
                }
                for(int i = 0; i < lista.size(); i++){
                    JLabel picLabel;
                    DistanceWeapon dw = (DistanceWeapon) lista.get(i);
                    picLabel = new JLabel(((DistanceWeapon)lista.get(i)).getImage());
                    picLabel.setBorder(new LineBorder(Color.black));
                    picLabel.setText(((DistanceWeapon)lista.get(i)).getName());
                    picLabel.setHorizontalTextPosition(JLabel.CENTER);
                    picLabel.setVerticalTextPosition(JLabel.BOTTOM);
                    picLabel.setFont(new Font("Serif", Font.PLAIN, 10));
                    picLabel.addMouseListener(new MouseAdapter() {
                        @Override
                        public void mousePressed(MouseEvent evt){
                            character.setDistanceWeapon(dw);
                            //JOptionPane.showMessageDialog(null, dw.getName() + " foi selecionado!", "Sucesso!", JOptionPane.INFORMATION_MESSAGE);
                            me.dispatchEvent(new WindowEvent(me, WindowEvent.WINDOW_CLOSING));
                        }
                    });
                    jpn_Tmp.add(picLabel);
                }
                break;
            case WEAPON_ROD:
            case WEAPON_WAND:
                switch(typeEquipment){
                    case WEAPON_ROD:
                        lista = Database.getRods();
                        break;
                    case WEAPON_WAND:
                        lista = Database.getWands();
                        break;
                }
                for(int i = 0; i < lista.size(); i++){
                    JLabel picLabel;
                    MagicWeapon mw = (MagicWeapon) lista.get(i);
                    picLabel = new JLabel(((MagicWeapon)lista.get(i)).getImage());
                    picLabel.setBorder(new LineBorder(Color.black));
                    picLabel.setText(((MagicWeapon)lista.get(i)).getName());
                    picLabel.setHorizontalTextPosition(JLabel.CENTER);
                    picLabel.setVerticalTextPosition(JLabel.BOTTOM);
                    picLabel.setFont(new Font("Serif", Font.PLAIN, 10));
                    picLabel.addMouseListener(new MouseAdapter() {
                        @Override
                        public void mousePressed(MouseEvent evt){
                            character.setMagicWeapon(mw);
                            //JOptionPane.showMessageDialog(null, mw.getName() + " foi selecionado!", "Sucesso!", JOptionPane.INFORMATION_MESSAGE);
                            me.dispatchEvent(new WindowEvent(me, WindowEvent.WINDOW_CLOSING));
                        }
                    });
                    jpn_Tmp.add(picLabel);
                }
                break;
            case SHIELD:
            case SPELLBOOK:
                switch(typeEquipment){
                    case SHIELD:
                        lista = Database.getShields();
                        break;
                    case SPELLBOOK:
                        lista = Database.getSpellbooks();
                        break;
                }
                for(int i = 0; i < lista.size(); i++){
                    JLabel picLabel;
                    Shield shield = (Shield) lista.get(i);
                    picLabel = new JLabel(((Shield)lista.get(i)).getImage());
                    picLabel.setBorder(new LineBorder(Color.black));
                    picLabel.setText(((Shield)lista.get(i)).getName());
                    picLabel.setHorizontalTextPosition(JLabel.CENTER);
                    picLabel.setVerticalTextPosition(JLabel.BOTTOM);
                    picLabel.setFont(new Font("Serif", Font.PLAIN, 10));
                    picLabel.addMouseListener(new MouseAdapter() {
                        @Override
                        public void mousePressed(MouseEvent evt){
                            character.setShield(shield);
                            //JOptionPane.showMessageDialog(null, shield.getName() + " foi selecionado!", "Sucesso!", JOptionPane.INFORMATION_MESSAGE);
                            me.dispatchEvent(new WindowEvent(me, WindowEvent.WINDOW_CLOSING));
                        }
                    });
//                    tam_x_grid = Math.max(tam_x_grid, picLabel.getFont().getSize());
                    jpn_Tmp.add(picLabel);
                }
                break;
            case HELMET:
            case ARMOR:
            case LEGS:
            case BOOTS:
                switch(typeEquipment){
                    case HELMET:
                        lista = Database.getHelmets();
                        break;
                    case ARMOR:
                        lista = Database.getArmors();
                        break;
                    case LEGS:
                        lista = Database.getLegs();
                        break;
                    case BOOTS:
                        lista = Database.getBoots();
                        break;
                }
                for(int i = 0; i < lista.size(); i++){
                    JLabel picLabel;
                    DefenseSet ds = (DefenseSet) lista.get(i);
                    picLabel = new JLabel(((DefenseSet)lista.get(i)).getImage());
                    picLabel.setBorder(new LineBorder(Color.black));
                    picLabel.setText(((DefenseSet)lista.get(i)).getName());
                    picLabel.setHorizontalTextPosition(JLabel.CENTER);
                    picLabel.setVerticalTextPosition(JLabel.BOTTOM);
                    picLabel.setFont(new Font("Serif", Font.PLAIN, 10));
                    picLabel.addMouseListener(new MouseAdapter() {
                        @Override
                        public void mousePressed(MouseEvent evt){
                            switch(typeEquipment){
                                case HELMET:
                                    character.setHelmet(ds);
                                    break;
                                case ARMOR:
                                    character.setArmor(ds);
                                    break;
                                case LEGS:
                                    character.setLegs(ds);
                                    break;
                                case BOOTS:
                                    character.setBoots(ds);
                                    break;
                            }
                            //JOptionPane.showMessageDialog(null, ds.getName() + " foi selecionado!", "Sucesso!", JOptionPane.INFORMATION_MESSAGE);
                            me.dispatchEvent(new WindowEvent(me, WindowEvent.WINDOW_CLOSING));
                        }
                    });
                    jpn_Tmp.add(picLabel);
                }
                break;
            case AMULET:
            case RING:
                switch(typeEquipment){
                    case AMULET:
                        lista = Database.getAmulets();
                        break;
                    case RING:
                        lista = Database.getRings();
                        break;
                }
                for(int i = 0; i < lista.size(); i++){
                    JLabel picLabel;
                    RingAmulet ra = (RingAmulet) lista.get(i);
                    picLabel = new JLabel(((RingAmulet)lista.get(i)).getImage());
                    picLabel.setBorder(new LineBorder(Color.black));
                    picLabel.setText(((RingAmulet)lista.get(i)).getName());
                    picLabel.setHorizontalTextPosition(JLabel.CENTER);
                    picLabel.setVerticalTextPosition(JLabel.BOTTOM);
                    picLabel.setFont(new Font("Serif", Font.PLAIN, 10));
                    picLabel.addMouseListener(new MouseAdapter() {
                        @Override
                        public void mousePressed(MouseEvent evt){
                            switch(typeEquipment){
                                case AMULET:
                                    character.setAmulet(ra);
                                    break;
                                case RING:
                                    character.setRing(ra);
                                    break;
                            }
                            //JOptionPane.showMessageDialog(null, ra.getName() + " foi selecionado!", "Sucesso!", JOptionPane.INFORMATION_MESSAGE);
                            me.dispatchEvent(new WindowEvent(me, WindowEvent.WINDOW_CLOSING));
                        }
                    });
                    jpn_Tmp.add(picLabel);
                }
                break;
            case AMMO_ARROW:
            case AMMO_BOLT:
                switch(typeEquipment){
                    case AMMO_ARROW:
                        lista = Database.getArrows();
                        break;
                    case AMMO_BOLT:
                        lista = Database.getBolts();
                        break;
                }
                for(int i = 0; i < lista.size(); i++){
                    JLabel picLabel;
                    Ammo ammo = (Ammo) lista.get(i);
                    picLabel = new JLabel(((Ammo)lista.get(i)).getImage());
                    picLabel.setBorder(new LineBorder(Color.black));
                    picLabel.setText(((Ammo)lista.get(i)).getName());
                    picLabel.setHorizontalTextPosition(JLabel.CENTER);
                    picLabel.setVerticalTextPosition(JLabel.BOTTOM);
                    picLabel.setFont(new Font("Serif", Font.PLAIN, 10));
                    picLabel.addMouseListener(new MouseAdapter() {
                        @Override
                        public void mousePressed(MouseEvent evt){
                            character.setAmmo(ammo);
                            //JOptionPane.showMessageDialog(null, ammo.getName() + " foi selecionado!", "Sucesso!", JOptionPane.INFORMATION_MESSAGE);
                            me.dispatchEvent(new WindowEvent(me, WindowEvent.WINDOW_CLOSING));
                        }
                    });
                    jpn_Tmp.add(picLabel);
                }
                break;
            case BACKPACK:
//                path = "Imagens\\Backpacks";
                break;
        }
        /* Deixar na quantidade mínima de 4 linhas para não bugar o GridLayout */
        for(int i = lista.size(); i < 13; i++){
            JLabel picLabel = new JLabel();
            jpn_Tmp.add(picLabel);
        }
        jpn_Imagens.setViewportView(jpn_Tmp);
    }
    
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jtx_nome_Item = new javax.swing.JTextField();
        lbl_nome_Item = new javax.swing.JLabel();
        jpn_Imagens = new javax.swing.JScrollPane();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setResizable(false);
        setSize(new java.awt.Dimension(450, 450));
        addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowClosing(java.awt.event.WindowEvent evt) {
                closing(evt);
            }
        });

        lbl_nome_Item.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        lbl_nome_Item.setText("Nome do item:");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(jpn_Imagens))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(0, 10, Short.MAX_VALUE)
                        .addComponent(lbl_nome_Item)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jtx_nome_Item, javax.swing.GroupLayout.PREFERRED_SIZE, 401, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jtx_nome_Item, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lbl_nome_Item))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jpn_Imagens, javax.swing.GroupLayout.PREFERRED_SIZE, 253, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void closing(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_closing
        // TODO add your handling code here:
        if(parent_Window != null){
            //parent_Window.dispatchEvent(new WindowEvent(parent_Window, WindowEvent.WINDOW_GAINED_FOCUS));
            parent_Window.setEnabled(true);
        }
    }//GEN-LAST:event_closing

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Selecao_Itens_Character.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Selecao_Itens_Character.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Selecao_Itens_Character.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Selecao_Itens_Character.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                try {
                    new Selecao_Itens_Character().setVisible(true);
                } catch (IOException ex) {
                    Logger.getLogger(Selecao_Itens_Character.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JScrollPane jpn_Imagens;
    private javax.swing.JTextField jtx_nome_Item;
    private javax.swing.JLabel lbl_nome_Item;
    // End of variables declaration//GEN-END:variables
}
